# Desktop Dungeons Java Framework #

This is a framework that interoperates with the Desktop Dungeons API (https://bitbucket.org/woitee/desktopdungeons_api) to provide an easy and comfortable way to create AI for Desktop Dungeons.

For example usage, see https://bitbucket.org/woitee/desktop-dungeons-bots.