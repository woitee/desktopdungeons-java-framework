package cz.woitee.ddframework;

import java.awt.Point;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import cz.woitee.ddframework.Pickup.Type;


/**
 * An example usage of the DDApi. Will be extended as the project continues.
 * 
 * @author woitee
 */

@SuppressWarnings("unused")
class Example {
	protected static void onException(Exception e) {
		e.printStackTrace();
	}
	
	public static void main(String[] args) {
		String exampleLevelFilename = "testLevel.txt";
		DDApi ddApi = new DDApi();
		// Whatever I'm trying out right now
		/* */
		try {
			ddApi.connect();
			try {
				DDState state = ddApi.getState(true);
				List<Point> moves = state.getExplorationMoves();
				while (!moves.isEmpty()) {
					DDState detached = state.detach();
					
					String lastAction = "";
					
					Random rand = new Random();
					if (state.getPickups().size() > 0) {
						lastAction = "PICKUP " + state.getPickups().get(0);
						state.pickup(state.getPickups().get(0));
						detached.pickup(detached.getPickups().get(0));
					} else {
						int ix = rand.nextInt(moves.size());
						lastAction = "MOVE " + moves.get(ix);
						state.walkTo(moves.get(ix));
						detached.walkTo(moves.get(ix));
					}
					
					// Freshly gained state from TCP connection
					DDState fresh = ddApi.getFreshState(true);
					
					if (!detached.equals(fresh)) {
						System.out.println("Simulation discrepancy detected!");
						System.out.println("After action: " + lastAction);
						
						System.out.println("STATE");
						System.out.println(fresh.toString());
						System.out.println("DETACHED");
						System.out.println(detached.toString());
						
						detached.equals(fresh);
					}
					
					moves = state.getExplorationMoves();
				}
				while (state.getPickups().size() > 0) {
					state.pickup(state.getPickups().get(0));
					continue;
				}
			} finally {
				ddApi.close();
			}
		} catch (Exception e) {
			onException(e);
		}
		/* */
		// Exploration bot
		/* *
		try {
			ddApi.connect();
			try {
				DDState state = ddApi.getState();
				List<Point> moves = state.getExplorationMoves();
				while (!moves.isEmpty()) {
					Thread.sleep(250);
					Random rand = new Random();
					int ix = rand.nextInt(moves.size());
					state.walkTo(moves.get(ix));
					moves = state.getExplorationMoves();
				}
			} finally {
				ddApi.close();
			}
		} catch (Exception e) {
			onException(e);
		}
		/* */
		
		// Setting custom dungeon
		/* *
		try {
			System.out.println("Trying to connect");
			ddApi.connect();
			try {
				System.out.println("Connected, sending layout");
				ddApi.setDungeonLayoutFromFile(exampleLevelFilename);
				System.out.println("Layout sent");
			} finally {
				ddApi.close();
			}
		} catch (IOException e) {
			onException(e);
		}
		/* */
		
		// Setting custom beasts
		/* *
		try {
			System.out.println("Trying to connect");
			ddApi.connect();
			try {
				System.out.println("Connected, sending beastlist");
				Beast.Type[] beasts = new Beast.Type[]{
						Beast.Type.AnimatedArmor,
						Beast.Type.Wraith
				};
				ddApi.setGeneratedBeasts(Arrays.asList(beasts));
			} finally {
				ddApi.close();
			}
		} catch (IOException e) {
			onException(e);
		}
		/* */
		
		// Trying game screen recognition
		/* *
		try {
			System.out.println("Trying to connect");
			ddApi.connect();
			try {
				System.out.println("Connected, asking for screen");
				System.out.println(ddApi.getCurrentScreen());
			} finally {
				ddApi.close();
			}
		} catch (IOException e) {
			onException(e);
		}
		/* */
		
		//Trying to start game
		/* *
		try {
			System.out.println("Trying to connect");
			ddApi.connect();
			try {
				System.out.println("Connected, trying to start game");
				ddApi.startGame(Hero.Class.Transmuter,
								Hero.Race.Dwarf,
								GameType.SnakePit,
								false);
			} finally {
				ddApi.close();
			}
		} catch (IOException e) {
			onException(e);
		}
		/* */
	}
}
