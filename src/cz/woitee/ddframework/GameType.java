package cz.woitee.ddframework;

/**
 * Enumerates possible game types.
 * 
 * @author woitee
 */

public enum GameType {
	Normal("NORMAL"),
	SnakePit("SNAKEPIT"),
	Library("LIBRARY"),
	Crypt("CRYPT"),
	Factory("FACTORY");
	
	private String name;
	private GameType(String name) {
		this.name = name;
	}
	
	/**
	 * Overriden, returns a string ready to send over the connection to Desktop Dungeons.
	 */
	@Override
	public String toString() {
		return name;
	}
}
