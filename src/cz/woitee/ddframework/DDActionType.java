package cz.woitee.ddframework;

/**
 * Enumeration of all game actions.
 * 
 * @author woitee
 */

public enum DDActionType {
	WORSHIP,
	ATTACK,
	WALK,
	USEGLYPH,
	CONVERTGLYPH,
	PICKUPGLYPH,
	PICKUP,
	BUYITEM,
	DRINKPOTION
}
