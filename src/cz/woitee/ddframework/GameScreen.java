package cz.woitee.ddframework;

/**
 * Enumeration of all the screens the game can be in.
 * @author Vojtech Cerny
 *
 */
public enum GameScreen {
	Menu, Game, RetireScreen;
}
