package cz.woitee.ddframework;

import java.awt.Point;
import java.io.IOException;


/**
 * A state of the game, that is currently attached to
 * the API. Every action taken will be performed in-game.
 * @author woitee
 *
 */

public class AttachedDDState extends DetachedDDState {
	protected ICommunicator apiComm;
	
	
	/**
	 * Creates a new AttachedDDState.
	 * @param apiComm Communicator representing connection to Desktop Dungeons.
	 * @throws IOException 
	 */
	public AttachedDDState(ICommunicator apiComm, boolean cheatingVision) throws IOException {
		this.apiComm = apiComm;
		super.setCheatingVision(cheatingVision);
		obtainGameObjects();
	}
	
	protected void obtainGameObjects() throws IOException {
		for (int x = 0; x < 20; ++x) {
			for (int y = 0; y < 20; ++y) {
				send("WHATSAT "+x+" "+y);
				String s = receive();
				DDObject received = DDObject.parse(s, new Point(x, y));
				if (received == null) {
					throw new IOException("Unknown message received after WHATSAT: " + s);
				}
				gameObjects.set(x, y, received);
			}
		}
		if (isCheatingVision()) {
			hiddenFields = new DDObject[20][20];
			for (int x = 0; x < 20; ++x) {
				for (int y = 0; y < 20; ++y) {
					send("CHEATWHATSAT " +x+" "+y);
					String s = receive();
					hiddenFields[x][y] = DDObject.parse(s, new Point(x, y));
				}
			}
		}
		// Update navigation
		navigation = new Navigation(gameObjects);
		// Get hero details
		gameObjects.getHero().getAdditionalInfoFrom(apiComm);
	}
	
	@Override
	protected DDObject discover(int x, int y) throws IOException {
		if (isCheatingVision()) {
			return super.discover(x, y);
		} else {
			send("WHATSAT "+x+" "+y);
			return DDObject.parse(receive(), new Point(x, y));
		}
	}
	
	protected void send(String message) throws IOException {
		apiComm.send(message);
	}
	
	protected String receive() throws IOException {
		return apiComm.receive();
	}
	
	@Override
	public boolean isAttached() {
		return true;
	}

	@Override
	public DDState detach() {
		return makeCopy();
	}

	@Override
	public DDState makeCopy() {
		DetachedDDState copy = null;
		if (isCheatingVision()) {
			copy = new DetachedDDState(gameObjects.makeCopy(), DDUtils.deepCopy(hiddenFields));
		} else {
			copy = new DetachedDDState(gameObjects.makeCopy());
		}	
		copy.bossSlain = bossSlain;
		return copy;
	}

	@Override
	public void worship(Altar altar) throws IOException {
		if (gameObjects.getHero().location != altar.location)
			send("GOTO " + altar.location.x + " " + altar.location.y);
		send("WORSHIP");
	}

	@Override
	protected void innerWalkTo(Point point) throws IOException {
		send("GOTO " + point.x + " " + point.y);
		super.innerWalkTo(point);
	}

	protected void innerAttack(Beast monster) throws IOException {
		send("GOTO " + monster.location.x + " " + monster.location.y);
		super.innerAttack(monster);
	}
	
	@Override
	public void useGlyph(Glyph glyph) throws IOException {
		if (!glyph.isHeld())
			return;
		
		int slot = getHero().getSlotOf(glyph);
		if (slot == -1)
			return;
		
		super.useGlyph(glyph);
		send("USEGLYPH " + slot);
	}

	@Override
	public void useGlyph(Glyph glyph, DDObject target) throws IOException {
		if (!glyph.isHeld())
			return;
		
		int slot = getHero().getSlotOf(glyph);
		if (slot == -1)
			return;
		
		super.useGlyph(glyph, target);
		send("USEGLYPH " + slot + " " + target.location.x + " " + target.location.y);
	}

	@Override
	public void convertGlyph(Glyph glyph) throws IOException {
		if (!glyph.isHeld())
			return;
		int slot = getHero().getSlotOf(glyph);
		if (slot == -1)
			return;
		
		super.convertGlyph(glyph);
		send("CONVERTGLYPH " + slot);
	}

	@Override
	public void pickupGlyph(Glyph glyph) throws IOException {
		if (!isReachable(glyph) || !getHero().hasFreeGlyphSlot())
			return;
		super.pickupGlyph(glyph);
		send("PICKUPGLYPH");
	}
	@Override
	public void buyItem(Shop shop) throws IOException {	
	}
	@Override
	public void drinkHealthPotion() throws IOException {
		if (getHero().getHPotions() <= 0)
			return;
		
		super.drinkHealthPotion();
		send("HPOTION");
	}
	@Override
	public void drinkManaPotion() throws IOException {
		if (getHero().getMPotions() <= 0)
			return;
		
		super.drinkManaPotion();
		send("MPOTION");
	}

	@Override
	public void setCheatingVision(boolean value) throws IOException {
		super.setCheatingVision(value);
		if (value)
			obtainGameObjects();
	}
}
