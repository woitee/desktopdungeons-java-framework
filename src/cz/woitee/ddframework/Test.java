package cz.woitee.ddframework;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import javax.swing.*;

import cz.woitee.websockets.*;
import cz.woitee.websockets.utils.UTF8String;

/**
 * Development testing package, will NOT be included in the final product.
 * 
 * @author woitee
 *
 */
class Test {
	JLabel incoming = new JLabel();
	JTextField outgoing = new JTextField();
	JButton btnSend = new JButton("Send");
	
	public Test() {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShowGUI();
			}
		});
		startServer();
	}
	
	private void onException(Exception e) {
		System.err.println(e.getMessage());
	}
	private void createAndShowGUI() {
		JFrame frame = new JFrame("WebSockets");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container cont = frame.getContentPane();
		cont.setLayout(new GridLayout(0, 1));
		
		cont.add(incoming);
		
		Dimension dim = outgoing.getPreferredSize();
		dim.width = 200;
		outgoing.setPreferredSize(dim);
		outgoing.setEnabled(false);
		outgoing.setText("waiting for connection");
		
		Box box = new Box(BoxLayout.X_AXIS);
		cont.add(box);
		box.add(outgoing);
		box.add(btnSend);
		//erase text on focus
		outgoing.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent arg0) {
			}
			@Override
			public void focusGained(FocusEvent arg0) {
				outgoing.setText("");
			}
		});
		
		
		frame.pack();
		frame.setVisible(true);
	}
	private void startServer() {
		ServerWebSocket swSocket = null;
		WebSocket socket = null;
		
		try {
			InetAddress addr = InetAddress.getByName(null);
			int port = 11854;
			swSocket = new ServerWebSocket(port, 10, addr);
			try {
				while (true) {
					incoming.setText("Starting to listen on port: "+port);
					socket = swSocket.accept();
					incoming.setText("Connection accepted");
					try {
						handleConnection(socket);
					} finally {
						socket.close();
					}
				}
			} catch (IOException e) {
				onException(e);
			} finally {
				swSocket.close();
			}
		} catch (IOException e) {
			onException(e);
		}
	}
	private void handleConnection(WebSocket socket) throws IOException {
		incoming.setText("Connection established.");
		outgoing.setEnabled(true);
		outgoing.setText("");
		
		final OutputStream output = socket.getOutputStream();
		final InputStream input = socket.getInputStream();
		//handle outgoing messages
		ActionListener btnSender = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					output.write(new UTF8String(outgoing.getText().toUpperCase()).array());
				} catch (IOException e) {
					onException(e);
				}
			}
		};
		KeyListener enterKeySender = new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent evt) {
				if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
					try {
						output.write(new UTF8String(outgoing.getText().toUpperCase()).array());
						outgoing.setText("");
					} catch (IOException e) {
						onException(e);
					}
				}
			}
		};
		try {
			btnSend.addActionListener(btnSender);
			outgoing.addKeyListener(enterKeySender);
		
			//handle incoming messages
			String line;
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));
			try {
				while ((line = reader.readLine()) != null) {
					incoming.setText(line);
				}
			} catch (IOException e) {
				onException(e);
			}
		} finally {
			btnSend.removeActionListener(btnSender);
			outgoing.removeKeyListener(enterKeySender);
			outgoing.setEnabled(false);
		}
	}
	
	public static void main(String[] args) {
		new Test();
	}
}
