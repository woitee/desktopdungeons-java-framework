package cz.woitee.ddframework;



import java.awt.Point;
import java.io.Serializable;

/**
 * A spell glyph object of the game. Can be either lying in the dungeon,
 * or held by the hero.
 * @author Vojtech Cerny
 *
 */
public class Glyph extends DDObject implements Serializable {
	private static final long serialVersionUID = 1L;
	private Type type;

	/**
	 * Constructor of glyph's lying on the floor in-game.
	 * @param type
	 * @param location
	 */
	public Glyph(Type type, Point location) {
		super(location);
		this.type = type;
		objectType = DDObjectType.GLYPH;
	}
	/**
	 * Constructor of glyphs in a hero's slot.
	 * @param type
	 */
	public Glyph(Type type) {
		this(type, new Point(-1, -1));
	}
	/**
	 * Type of the glyph, it's original title.
	 * @author Vojtech Cerny
	 *
	 */
	public enum Type {
		APHEELSIK,
		BLUDTUPOWA,
		BURNDAYRAZ,
		BYSSEPS,
		CYDSTEPP,
		ENDISWAL,
		GETINDARE,
		HALPMEH,
		IMAWAL,
		LEMMISI,
		PISORF,
		WEYTWUT,
		WONAFYT
	}
	/**
	 * Targetability mode of the glyph.
	 * @author Vojtech Cerny
	 *
	 */
	public enum Target {
		NONE,
		BEAST,
		WALL
	}
	
	public Type getGlyphType() {
		return type;
	}
	public Target getTargetType() {
		switch (getGlyphType()) {
		case APHEELSIK:
			return Target.BEAST;
		case BURNDAYRAZ:
			return Target.BEAST;
		case ENDISWAL:
			return Target.WALL;
		case IMAWAL:
			return Target.BEAST;
		case PISORF:
			return Target.BEAST;
		default:
			return Target.NONE;
		}
	}
	
	@Override
	public Object clone() {
		return new Glyph(type, (Point)location.clone());
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Glyph)) {
			return false;
		}
		
		Glyph other = (Glyph)o;
		return  this.location.equals(other.location) &&
				this.type == other.type;
	}
	
	/**
	 * Returns whether the glyph is currently held by hero.
	 * @return
	 */
	public boolean isHeld() {
		return location.x == -1 && location.y == -1;
	}
	
	@Override
	public String toString() {
		if (!isHeld()) {
			return "Glyph " + type + " at " + location;
		} else {
			return "Glyph " + type + " held by hero";
		}
	}
	
	@Override
	public String toLayoutString() {
		return "G_" + type.toString().substring(0, 3);
	}
	
	/**
	 * Parses a glyph that is in hand.
	 * @param s
	 * @return
	 */
	public static Glyph parse(String s) {
		return innerParse(s, new Point(-1, -1), true);
	}
	/**
	 * Parses a glyph that is lying on the floor in-game.
	 * @param s
	 * @param location
	 * @return
	 */
	public static Glyph parse(String s, Point location) {
		return innerParse(s, location, false);
	}
	private static Glyph innerParse(String s, Point location, boolean isInHand) {
		String[] split = s.split(" ");
		if (split.length != 2 || !split[0].equals("GLYPH"))
			return null;
		
		for (Type type: Type.values()) {
			if (type.toString().equals(split[1])) {
				if (isInHand) {
					return new Glyph(type);
				} else {
					return new Glyph(type, location);
				}
			}
		}
		return null;
	}
}
