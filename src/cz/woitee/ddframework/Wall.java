package cz.woitee.ddframework;

import java.awt.Point;
import java.io.Serializable;

/**
 * A wall object in the game.
 * @author Vojtech Cerny
 *
 */
public class Wall extends DDObject implements Serializable {
	private static final long serialVersionUID = 1L;

	public Wall(Point location) {
		super(location);
		objectType = DDObjectType.WALL;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Wall)) {
			return false;
		}
		
		Wall other = (Wall)o;
		return  this.location.equals(other.location);
	}
	
	@Override
	public Object clone() {
		return new Wall((Point)location.clone());
	}
	
	@Override
	public String toString() {
		return "Wall at " + location;
	}
	
	@Override
	public String toLayoutString() {
		return "#";
	}
	
	public static Wall parse(String s, Point location) {
		if (s.equals("WALL"))
			return new Wall(location);
		return null;
	}
}
