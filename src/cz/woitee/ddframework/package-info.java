/**
 * Package containing all necessary objects for creating custom
 * bots for Desktop Dungeons.
 */

package cz.woitee.ddframework;