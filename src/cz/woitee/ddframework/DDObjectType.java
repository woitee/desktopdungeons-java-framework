package cz.woitee.ddframework;

/**
 * Listing of object types that can occupy a space in the dungeon.
 * @author woitee
 *
 */

public enum DDObjectType {
	HERO,
	BEAST,
	GLYPH,
	EMPTY,
	ALTAR,
	SHOP,
	DARKNESS,
	WALL,
	PICKUP //potions and stat enhancers, gold
}
