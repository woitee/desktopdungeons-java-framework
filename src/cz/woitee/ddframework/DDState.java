package cz.woitee.ddframework;

import java.awt.Point;
import java.io.IOException;
import java.util.List;

import cz.woitee.ddframework.DetachedDDState.HeroAndBeast;

/**
 * Interface describing a game state. The state can be attached or detached,
 * describing whether the called methods will affect the state of the game.
 * 
 * @author woitee
 */

public interface DDState {
	/** Set to true if you want to be able to access objects at unrevealed locations. */
	public void setCheatingVision(boolean value) throws IOException;
	/** Returns whether the API is set to be able to access objects at unrevealed locations.*/
	public boolean isCheatingVision();
	
	/**
	 * Describes whether the state is attached, which means that actions
	 * will be performed in-game. Detached states are only "simulations".
	 * @return
	 */
	public boolean isAttached();
	/**
	 * Returns a detached state that is either the same instance,
	 * if the state already was detached, or a new copy of the state, detached.
	 * A detached state will only
	 * simulate actions called and won't perform them in-game.
	 * 
	 * If you want to create a different instance either way, use makeCopy().
	 * @return 
	 */
	public DDState detach();
	/**
	 * Performs a deep-copy of the object into a detached DDState.
	 * @return
	 */
	public DDState makeCopy();
	
	//getting game info
	/**
	 * Returns a game object on given coordinates. If cheating vision
	 * is enabled, this method will never return the Darkness object.
	 * 
	 * @param x X-coordinate of the object (left-to-right, zero-based).
	 * @param y Y-coordinate of the object (top-to-bottom, zero-based).
	 * @return
	 */
	public DDObject getObjectAt(int x, int y);
	/**
	 * Returns a game object on given coordinates. If cheating vision
	 * is enabled, this method will never return the Darkness object.
	 * 
	 * @param point Coordinates of the object.
	 * @return
	 */
	public DDObject getObjectAt(Point point);
	/**
	 * Returns a two-dimensional array of the in-game objects. IF cheating
	 * vision is enabled, there will be no Darkness objects in the array.
	 * @return
	 */
	public GameObjects getObjects();
	/**
	 * Returns the same object as passed from this state.
	 * Useful when working with multiple (i.e. attached and detached) states,
	 * and one wants to do mirror actions on both.
	 * @param ddObject
	 * @return
	 */
	public <T extends DDObject> T getEquivalent (T ddObject);
	/**
	 * Returns a Collection of all seen Altars.
	 * @return
	 */
	public List<Altar> getAltars();
	/**
	 * Returns a Collection of all seen Monsters.
	 * @return
	 */
	public List<Beast> getBeasts();
	/**
	 * Returns a Collection of all seen Glyphs.
	 * @return
	 */
	public List<Glyph> getGlyphs();
	/**
	 * Returns a Collection of all seen Pickups.
	 * @return
	 */
	public List<Pickup> getPickups();
	/**
	 * Returns a Collection of all seen Shops.
	 * @return
	 */
	public List<Shop> getShops();
	/**
	 * Returns a Collection of all doable actions, that have an impact on the game.
	 * (Walks to places that don't reveal any location won't be listed)
	 * @return
	 */
	public List<DDAction> getActions();
	/**
	 * Return all Empty locations that can be walked to and will reveal a location.
	 * @return
	 */
	public List<Point> getExplorationMoves();
	/**
	 * Returns all neighboring points of a point.
	 * @param point
	 * @return
	 */
	public List<Point> getNeighbors(Point point);
	/**
	 * Returns the hero instance.
	 * @return
	 */
	public Hero getHero();
	
	/**
	 * Asks whether a point in the dungeon is reachable from hero.
	 * @param point the point we are trying to reach.
	 * @return true if the point can be reached.
	 */
	public boolean isReachable(Point point);
	
	/**
	 * Asks whether a DDObject is reachable from hero.
	 * @param object the object we are trying to reach.
	 * @return true if object can be reached.
	 */
	public boolean isReachable(DDObject object);
	
	//interact with the game through the related objects
	/**
	 * Worship a god of an altar. Only possible if the hero has no god, or
	 * the god accepts conversion from the current one.
	 * @param altar the altar of god we want to worship.
	 * @throws IOException 
	 */
	public void worship(Altar altar) throws IOException;
	/**
	 * Attack a monster.
	 * @param monster the monster we want to attack.
	 */
	public void attack(Beast monster) throws IOException;
	/**
	 * Predicts the result of an attack.
	 * @param beast
	 * @return
	 */
	public HeroAndBeast attackPrediction(Beast beast);
	/**
	 * Walk to an empty location.
	 * @param empty the empty location we want to go to.
	 */
	public void walkTo(Point point) throws IOException;
	/**
	 * Use a glyph that is owned.
	 * @param glyph
	 */
	public void useGlyph(Glyph glyph) throws IOException;
	/**
	 * Use a glyph on a target.
	 * @param glyph
	 * @param target
	 */
	public void useGlyph(Glyph glyph, DDObject target) throws IOException;
	/**
	 * Convert a glyph for racial bonus.
	 * @param glyph
	 */
	public void convertGlyph(Glyph glyph) throws IOException;
	/**
	 * Pickup a glyph off the ground.
	 * @param glyph
	 */
	public void pickupGlyph(Glyph glyph) throws IOException;
	/**
	 * Pickup a basic pickable item from the ground.
	 * @param pickup
	 */
	public void pickup(Pickup pickup) throws IOException;
	/**
	 * Buy an item from a shop.
	 * @param shop
	 */
	public void buyItem(Shop shop) throws IOException;
	/**
	 * Drink a health potion.
	 * @throws IOException
	 */
	public void drinkHealthPotion() throws IOException;
	/**
	 * Drink a mana potion.
	 * @throws IOException
	 */
	public void drinkManaPotion() throws IOException;
	/**
	 * Perform a generic action in the game.
	 * @param action
	 */
	public void doAction(DDAction action) throws IOException;
	public boolean isBossSlain();
}
