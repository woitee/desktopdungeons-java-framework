package cz.woitee.ddframework;

import java.io.IOException;

/**
 * Class representing the framework's exceptions. It extends IOException, which
 * most of Framework's methods throw anyway, so the user doesn't need to, but can, listen
 * for the rare IOExceptions separately.
 * 
 * @author woitee
 */
public class DDException extends IOException {
	private static final long serialVersionUID = 1L;
	
	public DDException() {
	}
	public DDException (String msg) {
		super(msg);
	}
	public DDException (Throwable cause) {
		super(cause);
	}
	public DDException (String msg, Throwable cause) {
		super(msg, cause);
	}
}
