package cz.woitee.ddframework;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;

import cz.woitee.websockets.*;

/**
 * Class providing methods for connecting to Desktop Dungeons API. 
 * Provides methods of accessing and manipulating the in-game data.
 * 
 * @author woitee
 *
 */

public class DDApi implements Closeable {
	/**
	 * A class used to send string messages directly to the game API.
	 * Should not be needed nor used, unless in very peculiar cases.
	 * @author Vojtech Cerny
	 */
	public class DirectAccess {
		public void send(String message) throws IOException {
			DDApi.this.send(message);
		}
		
		public String receive() throws IOException {
			return DDApi.this.receive();
		}
		
		public void trySend(String message) {
			try {
				DDApi.this.send(message);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		public String tryReceive() {
			try {
				return DDApi.this.receive();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
	}
	
	
	private ServerWebSocket serverSocket = null;
	private WebSocket socket = null;
	private final static int port = 11854;
	private Writer socketWriter = null;
	private BufferedReader socketReader = null;
	/**
	 * Send messages directly to the game. This can make the API and any of its
	 * AttachedDDStates incosistent. Use at your own risk. 
	 */
	public DirectAccess directAccess = new DirectAccess();
	
	/**
	 * Contains a state with information and full access to the game.
	 */
	protected AttachedDDState currentState = null;
	
	/**
	 * Creates a new instance of DDApi.
	 */
	public DDApi() {
	}
	
	/**
	 * Sends a message through the WebSocket connection.
	 * @param message A message that will be sent.
	 */
	private void send(String message) throws IOException {
		socketWriter.write(message);
		socketWriter.flush();
	}
	
	private String receive() throws IOException {
		return socketReader.readLine();
	}
	
	/**
	 * Connect to Desktop Dungeons API running at localhost.
	 * Blocks until connection is made. Should be called before any other
	 * methods are used.
	 * 
	 * @throws IOException if an I/O error occurs while connecting.
	 */
	public void connect() throws IOException {
		InetAddress localhost = null;
		try {
			localhost = InetAddress.getByName(null);
		} catch(UnknownHostException e) {
			//should never happen
			return;
		}
		
		serverSocket = new ServerWebSocket(port, 10, localhost);
		socket = serverSocket.accept();
		serverSocket.close();
		socketReader = new BufferedReader(
			new InputStreamReader(socket.getInputStream(),"UTF-8")
		);
		socketWriter = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
	}
	
	/**
	 * Closes the ongoing connection. Should be called once the object isn't needed anymore.
	 * @throws IOException
	 */
	public void close() throws IOException {
		socket.close();
	}
	
	/**
	 * Starts or restarts a default game, and returns an attached state of it. If an attached game
	 * state already exists, it will be detached. Use this method to access the game.
	 * @throws IOException 
	 */
	public DDState startGame(Hero.Clazz heroClass, Hero.Race heroRace, GameType gameType, boolean immortal, boolean cheatingVision) throws IOException {
		GameScreen screen = getCurrentScreen();
		if (screen == GameScreen.Game) {
			send("RETIRE");
			send("RETIRE");
		} else if (screen == GameScreen.RetireScreen) {
			send("RETIRE");
		}
		send(String.format("STARTGAME %s %s %s %s",
				heroClass, heroRace, gameType, immortal ? "TRUE" : "FALSE"));
		
		currentState = createCurrentState(cheatingVision);
		
		return currentState;
	}
	
	/**
	 * Attaches to game and gathers all the information from it. Call only after
	 * connection has been made. Returns a DDState with access to all the data.
	 * @throws IOException 
	 */
	protected AttachedDDState createCurrentState(boolean cheatingVision) throws IOException {
		//Creating a new DDState that can send and receive commands
		AttachedDDState state = new AttachedDDState(new ICommunicator() {	
			@Override
			public void send(String message) throws IOException {
				DDApi.this.send(message);
			}
					
			@Override
			public String receive() throws IOException {
				return DDApi.this.receive();
			}
		}, cheatingVision);
		return state;
	}
	
	/**
	 * Returns an attached state of the game. Only one attached state
	 * may exist at a time, so if it exists, it will be returned.
	 * Use this method to access the game.
	 * Will return null if the game isn't started.
	 * @param cheatingVision Whether the state should use cheating vision. 
	 */
	public DDState getState(boolean cheatingVision) throws IOException {
		// If the API connected mid-game, it needs to gather information into state now.
		if (currentState == null) {
			if (getCurrentScreen() == GameScreen.Game) {
				currentState = createCurrentState(cheatingVision);
			}
		}
		return currentState;
	}
	
	/**
	 * Gets a fresh state from the TCP connection to Desktop Dungeons.
	 * Also refreshes the current state returned by getState().
	 * Returns null if the game is in menu or retire screen.
	 * @param cheatingVision Whether the state should use cheating vision.
	 * @return
	 * @throws IOException
	 */
	public DDState getFreshState(boolean cheatingVision) throws IOException {
		if (getCurrentScreen() == GameScreen.Game) {
			return createCurrentState(cheatingVision);
		}
		return null;
	}
	/**
	 * Returns an attached state of the game, without cheating vision.
	 * Only one attached state
	 * may exist at a time, so if it exists, it will be returned.
	 * Use this method to access the game.
	 * Will return null if the game isn't started.
	 */
	public DDState getState() throws IOException {
		return getState(false);
	}
	
	/**
	 * Tells the API to only generate the selected monster types in the dungeons.
	 * @param monsters A list of monsters the game can put in the dungeon.
	 * @throws IOException 
	 */
	public void setGeneratedBeasts(Collection<Beast.Type> beasts) throws IOException {
		StringBuilder sb = new StringBuilder("SETBEASTS");
		for (Beast.Type beast: beasts) {
			sb.append(' ');
			sb.append(beast.toString());
		}
		send(sb.toString());
	}
	/**
	 * Tells the API to use a specific dungeon layout, instead of generating one.
	 * @param objects GameObjects instance, representing the dungeon layout.
	 * @throws IOException 
	 */
	public void setDungeonLayout(GameObjects objects) throws IOException {
		setDungeonLayout(objects.grid);
	}
	
	/**
	 * Tells the API to use a specific dungeon layout, instead of generating one.
	 * @param objects A 20x20 grid of game objects, representing the dungeon layout.
	 * @throws IOException 
	 */
	public void setDungeonLayout(DDObject[][] objects) throws IOException {
		StringBuilder sb = new StringBuilder("SETDUNGEON");
		for (int i = 0; i < 20; ++i) {
			for (int j = 0; j < 20; ++j) {
				sb.append(' ');
				sb.append(objects[j][i].toLayoutString());
			}
		}
		send(sb.toString());
	}
	
	/**
	 * Tells the API to use a specific dungeon layout, which it will
	 * parse from a given string.
	 * @param layout The string containing the dungeon layout. Has to be formatted
	 * 				 according to the DD API specification, but may additionally
	 * 				 contain any amount of extra whitespace.
	 * @throws IOException if an I/O error occurs while communicating with DD.
	 */
	public void setDungeonLayoutFromString(String layout) throws IOException {
		String validFormat = layout.replaceAll("\\s+", " ");
		validFormat = validFormat.trim();
		send("SETDUNGEON " + validFormat);
	}
	
	/**
	 * Tells the API to use a specific dungeon layout, which it will
	 * read from a file.
	 * @param layout The file containing the dungeon layout. Has to be a text file,
	 * 				 containing the layout formatted as specified by the DD API specification,
	 * 				 but may additionally contain any amount of extra whitespace.
	 * @throws FileNotFoundException if a file of that name wasn't found.
	 * @throws IOException if an I/O error occurs while reading from file, or while communicating with DD.
	 */
	public void setDungeonLayoutFromFile(String filename) throws FileNotFoundException, IOException {
		//Read file to end
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		StringBuilder sb = new StringBuilder();
		int c;
		while ((c = reader.read()) != -1) {
			sb.append((char) c);
		}
		reader.close();
		setDungeonLayoutFromString(sb.toString());
	}
	
	/**
	 * Tells the API to stop using custom dungeon layout and start to generate dungeons again.
	 */
	public void resetDungeonLayout() throws IOException {
		send("UNSETDUNGEON");
	}
	
	/**
	 * Asks whether the game is in the menu, in the game, or on the retire screen.
	 * @return
	 * @throws IOException
	 */
	public GameScreen getCurrentScreen() throws IOException {
		send("STATE");
		String s = receive();
		if (s.equals("MENU")) {
			return GameScreen.Menu;
		} else if (s.equals("GAME")) {
			return GameScreen.Game;
		} else if (s.equals("RETIRESCREEN")) {
			return GameScreen.RetireScreen;
		}
		//shouldn't reach here
		throw new DDException("Unexpected reply to STATE message received");
	}
	
	/**
	 * Retires current game, or exits retire screen. If the game is in menu, nothing happens.
	 */
	public void retire() throws IOException {
		currentState.detach();
		currentState = null;
		if (getCurrentScreen() != GameScreen.Menu)
			send("RETIRE");
	}
}
