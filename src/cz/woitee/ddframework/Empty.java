package cz.woitee.ddframework;

import java.awt.Point;
import java.io.Serializable;

/**
 * Object denoting an empty field in the game.
 * @author Vojtech Cerny
 */
public class Empty extends DDObject implements Serializable {
	private static final long serialVersionUID = 1L;

	public Empty(Point location) {
		super(location);
		objectType = DDObjectType.EMPTY;
	}
	
	@Override
	public Object clone() {
		return new Empty((Point)location.clone());
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Empty)) {
			return false;
		}
		
		Empty other = (Empty)o;
		return  this.location.equals(other.location);
	}
	
	@Override
	public String toString() {
		return "Empty at " + location;
	}
	
	@Override
	public String toLayoutString() {
		return "_";
	}
	
	public static Empty parse(String s, Point location) {
		// We do not want to care about blood nor signpost objects
		if (s.equals("EMPTY") || s.equals("BLOOD") || s.equals("SIGNPOST"))
			return new Empty(location);
		return null;
	}
}
