package cz.woitee.ddframework;

import java.io.IOException;

/**
 * An interface containing a bidirectional String messaging system.
 * 
 * @author woitee
 */
public interface ICommunicator {
	public void send(String message) throws IOException;
	public String receive() throws IOException;
}
