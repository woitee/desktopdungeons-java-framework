package cz.woitee.ddframework;

/**
 * A common predeccessor to all game actions. The classes have no methods,
 * they only describe the action properties.
 * 
 * @author woitee
 */
public abstract class DDAction {
	protected DDActionType actionType;
	DDActionType getType() {
		return actionType;
	}
	protected DDAction(DDActionType actionType) {
		this.actionType = actionType;
	}
}