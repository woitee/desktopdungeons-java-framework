package cz.woitee.ddframework.actions;

import cz.woitee.ddframework.DDAction;
import cz.woitee.ddframework.DDActionType;
import cz.woitee.ddframework.Shop;

/**
 * Action describing buying of an item.
 * @author Vojtech Cerny
 *
 */
public class BuyItem extends DDAction {
	public Shop shop;
	
	public BuyItem(Shop shop) {
		super(DDActionType.BUYITEM);
		this.shop = shop;
	}
	
	@Override
	public String toString() {
		return "DDAction.BUYITEM {Shop: " + shop + "}";
	}
}