package cz.woitee.ddframework.actions;

import java.awt.Point;

import cz.woitee.ddframework.DDAction;
import cz.woitee.ddframework.DDActionType;

/**
 * Action describing a walk to a specific location.
 * @author Vojtech Cerny
 *
 */
public class Walk extends DDAction {
	public Point point;
	public Walk(Point point) {
		super(DDActionType.WALK);
		this.point = point;
	}
	
	@Override
	public String toString() {
		return "DDAction.WALK {Point: (" + point.x + "," + point.y + ")}";
	}
}
