package cz.woitee.ddframework.actions;

import cz.woitee.ddframework.DDAction;
import cz.woitee.ddframework.DDActionType;
import cz.woitee.ddframework.Pickup;

/**
 * Action describing picking of a pickup.
 * @author Vojtech Cerny
 *
 */
public class PickUpPickup extends DDAction {
	public Pickup pickup;
	
	public PickUpPickup(Pickup pickup) {
		super(DDActionType.PICKUP);
		this.pickup = pickup;
	}
	
	@Override
	public String toString() {
		return "DDAction.PICKUP {Pickup: " + pickup + "}";
	}
}
