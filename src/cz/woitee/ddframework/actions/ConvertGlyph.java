package cz.woitee.ddframework.actions;

import cz.woitee.ddframework.DDAction;
import cz.woitee.ddframework.DDActionType;
import cz.woitee.ddframework.Glyph;

/**
 * Action describing converting a glyph.
 * @author Vojtech Cerny
 *
 */
public class ConvertGlyph extends DDAction {
	public Glyph glyph;
	public ConvertGlyph(Glyph glyph) {
		super(DDActionType.CONVERTGLYPH);
		this.glyph = glyph;
	}
	
	@Override
	public String toString() {
		return "DDAction.CONVERTGLYPH {Glyph: " + glyph + "}";
	}
}
