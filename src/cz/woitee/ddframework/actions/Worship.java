package cz.woitee.ddframework.actions;

import cz.woitee.ddframework.Altar;
import cz.woitee.ddframework.DDAction;
import cz.woitee.ddframework.DDActionType;

/**
 * Action describing worshipping a god.
 * @author Vojtech Cerny
 *
 */
public class Worship extends DDAction {
	public Altar altar;
	public Worship(Altar altar) {
		super(DDActionType.WORSHIP);
		this.altar = altar;
	}
	
	@Override
	public String toString() {
		return "DDAction.WORSHIP {Altar: " + altar + "}";
	}
}
