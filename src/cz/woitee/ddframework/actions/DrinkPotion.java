package cz.woitee.ddframework.actions;

import cz.woitee.ddframework.DDAction;
import cz.woitee.ddframework.DDActionType;

/**
 * Action of drinking a health or mana potion.
 * @author Vojtech Cerny
 *
 */
public class DrinkPotion extends DDAction {
	/*
	 * Whether this action means drinking a health or mana potion.
	 */
	public enum PotionKind {
		HEALTH, MANA
	}
	public PotionKind potionKind;
	public DrinkPotion(PotionKind potionKind) {
		super(DDActionType.DRINKPOTION);
		this.potionKind = potionKind;
	}
	
	@Override
	public String toString() {
		return "DDAction.DRINKPOTION {Kind: " + potionKind + "}";
	}
}
