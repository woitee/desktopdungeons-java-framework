package cz.woitee.ddframework.actions;

import cz.woitee.ddframework.DDAction;
import cz.woitee.ddframework.DDActionType;
import cz.woitee.ddframework.DDObject;
import cz.woitee.ddframework.Glyph;

/**
 * Action describing using of a glyph (casting a spell).
 * @author Vojtech Cerny
 *
 */
public class UseGlyph extends DDAction {
	public Glyph glyph;
	public DDObject target;
	public UseGlyph(Glyph glyph) {
		super(DDActionType.USEGLYPH);
		this.glyph = glyph;
	}
	public UseGlyph(Glyph glyph, DDObject target) {
		super(DDActionType.USEGLYPH);
		this.glyph = glyph;
		this.target = target;
	}
	
	@Override
	public String toString() {
		if (target == null)
			return "DDAction.USEGLYPH {Glyph: " + glyph + "}";
		else
			return "DDAction.USEGLYPH {Glyph: " + glyph + " target:" + target + "}";
	}
}
