package cz.woitee.ddframework.actions;

import cz.woitee.ddframework.Beast;
import cz.woitee.ddframework.DDAction;
import cz.woitee.ddframework.DDActionType;

/**
 * Action describing an attack on a beast.
 * @author Vojtech Cerny
 *
 */
public class Attack extends DDAction {
	public Beast beast;
	
	public Attack(Beast beast) {
		super(DDActionType.ATTACK);
		this.beast = beast;
	}
	
	@Override
	public String toString() {
		return "DDAction.ATTACK {Beast: " + beast + "}";
	}
}
