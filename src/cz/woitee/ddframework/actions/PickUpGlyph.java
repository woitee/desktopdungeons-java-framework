package cz.woitee.ddframework.actions;

import cz.woitee.ddframework.DDAction;
import cz.woitee.ddframework.DDActionType;
import cz.woitee.ddframework.Glyph;

/**
 * Action decribing picking of a glyph.
 * @author Vojtech Cerny
 *
 */
public class PickUpGlyph extends DDAction {
	public Glyph glyph;
	public PickUpGlyph(Glyph glyph) {
		super(DDActionType.PICKUPGLYPH);
		this.glyph = glyph;
	}
	
	@Override
	public String toString() {
		return "DDAction.PICKUPGLYPH {Glyph: " + glyph + "}";
	}
}
