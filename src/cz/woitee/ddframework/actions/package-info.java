/**
 * This package contains only objects extending the generic
 * DDAction class, that represent the various actions of the game.
 */

package cz.woitee.ddframework.actions;