package cz.woitee.ddframework;

import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.woitee.ddframework.Hero;
import cz.woitee.ddframework.Pickup.Type;
import cz.woitee.ddframework.actions.*;
import cz.woitee.ddframework.actions.DrinkPotion.PotionKind;

/**
 * A state of the game, that is currently detached from
 * the API. Every action taken will be merely simulated,
 * and will not affect the game running in the browser.
 * @author woitee
 *
 */

public class DetachedDDState implements DDState {
	public class HeroAndBeast {
		public Hero hero;
		public Beast beast;
		
		public HeroAndBeast(Hero hero, Beast beast) {
			this.hero = hero;
			this.beast = beast;
		}
	}
	
	protected boolean bossSlain = false;
	protected GameObjects gameObjects;
	protected DDObject[][] hiddenFields;
	protected Navigation navigation;
	private boolean cheatingVision = false;
	
	/**
	 * Default constructor, should never be used directly,
	 * only as a base in childs. Contains nulls instead of objects.
	 */
	public DetachedDDState() {
		gameObjects = new GameObjects(20, 20);
	}
	
	/**
	 * Creates a DetachedDDState with set objects in the game.
	 * What lies under darkness is not known and will appear as wall.
	 * @param gameObjects Objects in this state.
	 */
	public DetachedDDState(GameObjects gameObjects) {
		this.gameObjects = gameObjects;
		navigation = new Navigation(gameObjects);
		cheatingVision = false;
	}
	
	/**
	 * Creates a DetachedDDState with set objects in the game.
	 * Undiscovered objects are set also, to provide full game simulation.
	 * @param gameObjects Objects in this state. Can contain Darkness objects.
	 * @param hiddenFields All objects in this state. Should contain what's underneath
	 * 		  the darkness instead of Darkness objects.
	 */
	public DetachedDDState(GameObjects gameObjects, DDObject[][] hiddenFields) {
		this(gameObjects);
		this.hiddenFields = hiddenFields;
		cheatingVision = true;
	}
	
	@Override
	public boolean isAttached() {
		return false;
	}

	@Override
	public DDState detach() {
		return this;
	}
	@Override
	public DDState makeCopy() {
		DetachedDDState copy = new DetachedDDState(gameObjects.makeCopy());
		copy.bossSlain = bossSlain;
		return copy;
	}
	
	@Override
	public DDObject getObjectAt(int x, int y) {
		return gameObjects.get(x, y);
	}
	@Override
	public DDObject getObjectAt(Point point) {
		return getObjectAt(point.x, point.y);
	}
	@SuppressWarnings("unchecked")
	@Override
	public <T extends DDObject> T getEquivalent (T ddObject) {
		return (T) getObjectAt(ddObject.location);
	}
	/**
	 * Creates a revealed copy of gameObjects, using information in hiddenFields.
	 * Should be used only if cheatingVision == true;
	 * @return
	 */
	protected GameObjects createRevealedGameObjects() {
		GameObjects copy = gameObjects.makeCopy();
		for (int x = 0; x < 20; ++x) {
			for (int y = 0; y < 20; ++y) {
				if (gameObjects.get(x, y).getType() == DDObjectType.DARKNESS) {
					copy.set(x, y, hiddenFields[x][y]);
				}
			}
		}
		return copy;
	}
	@Override
	public GameObjects getObjects() {
		if (!cheatingVision)
			return gameObjects.makeCopy();
		else
			//return gameObjects.makeCopy();
			return createRevealedGameObjects();
	}

	@Override
	public List<Altar> getAltars() {
		return gameObjects.getAltars();
	}
	@Override
	public List<Beast> getBeasts() {
		return gameObjects.getBeasts();
	}
	@Override
	public List<Glyph> getGlyphs() {
		return gameObjects.getGlyphs();
	}
	@Override
	public List<Pickup> getPickups() {
		return gameObjects.getPickups();
	}
	@Override
	public List<Shop> getShops() {
		return gameObjects.getShops();
	}

	@Override
	public Hero getHero() {
		return gameObjects.getHero();
	}

	/**
	 * A function that is called after a Darkness object is explored, and should return
	 * what lies underneath.
	 * @param x Zero-based x-coordinate of the revealed location.
	 * @param y Zero-based y-coordinate of the revealed location.
	 * @return What was discovered.
	 * @throws IOException
	 */
	protected DDObject discover(int x, int y) throws IOException {
		if (cheatingVision) {
			return hiddenFields[x][y];
		} else {
			DDObject wall = new Wall(new Point(x, y));
			wall.location = new Point(x, y);
			return wall;
		}
	}
	/**
	 * A function that is called after a Darkness object is explored, and should return
	 * what lies underneath.
	 * @param point The revealed location coordinates.
	 * @return What was discovered.
	 * @throws IOException
	 */
	protected DDObject discover(Point point) throws IOException {
		return discover(point.x, point.y);
	}
	/**
	 * Discovers object at a location, and registers it into internal structures.
	 * @param x
	 * @param y
	 * @throws IOException
	 */
	private void discoverAndHandle(int x, int y) throws IOException {
		DDObject discovered = discover(x, y);
		gameObjects.set(x, y, discovered);
		navigation.updateLocation(x, y);
		
		Hero hero = gameObjects.getHero();
		// Get health regen
		int healthRegen = hero.level;
		if (hero.clazz == Hero.Clazz.Monk)
			healthRegen *= 2;
		
		// Regenerate hero
		if (!hero.isPoisoned())
			hero.gainHealth(healthRegen);
		if (!hero.isManaBurned())
			hero.gainMana(1);
		
		// Regenerate beasts
		for (Beast beast : gameObjects.getBeasts()) {
			if (!beast.isPoisoned())
				beast.gainHealth(beast.getLevel());
		}
	}
	private void discoverAndHandle(Point point) throws IOException {
		discoverAndHandle(point.x, point.y);
	}
	
	@Override
	public boolean isReachable(Point point) {
		return navigation.isReachable(point);
	}
	@Override
	public boolean isReachable(DDObject object) {
		return isReachable(object.location);
	}
	
	private Point neighborPoint(Point point) {
		for (Point neigh : navigation.getNeighbors(point)) {
			if (isReachable(neigh)) {
				boolean neighborsDarkness = false;
				for (Point neigh2 : navigation.getNeighbors(neigh)) {
					if (gameObjects.get(neigh2).objectType == DDObjectType.DARKNESS) {
						neighborsDarkness = true;
						break;
					}
				}
				if (!neighborsDarkness) {
					return neigh;
				}
			}
		}
		return new Point(-1, -1);
	}
	@Override
	public List<Point> getExplorationMoves() {
		List<Point> ret = new ArrayList<Point>();
		for (int x = 0; x < 20; ++x) {
			for (int y = 0; y < 20; ++y) {
				if (navigation.isReachable(x, y)) {
					boolean touchesDarkness = false;
					
					for (Point neigh : navigation.getNeighbors(x, y)) {
						if (gameObjects.get(neigh.x, neigh.y).objectType == DDObjectType.DARKNESS) {
							touchesDarkness = true;
							break;
						}
					}
					if (touchesDarkness) {
						ret.add(new Point(x, y));
					}
				}
			}
		}
		return ret;
	}
	public List<Point> getNeighbors(Point point) {
		return navigation.getNeighbors(point);
	}

	private void beastDealtDamage(Beast beast) {
		unpoisonBeasts();
		
		if (beast.isDead()) {
			gameObjects.set(beast.location, new Empty(beast.location));
			navigation.updateLocation(beast.location);
			if (beast.level == 10)
				bossSlain = true;
		}
	}
	protected void unpoisonBeasts() {
		for (Beast beast : getBeasts()) {
			beast.poisoned = false;
		}
	}
	@Override
	public void worship(Altar altar) throws IOException {
		
	}
	@Override
	public void attack(Beast monster) throws IOException {
		//Ensure the monster is from this state
		monster = getEquivalent(monster);
		if (!navigation.isTouchable(monster.location))
			return;
		
		Point move = neighborPoint(monster.location);
		
		walkTo(move);
		innerAttack(monster);
	}
	protected void innerAttack(Beast beast) throws IOException {
		Hero hero = gameObjects.getHero();
		hero.performAttack(beast);
		
		beastDealtDamage(beast);
	}
	public HeroAndBeast attackPrediction(Beast beast) {
		Hero heroCopy = DDUtils.deepCopy(getHero());
		Beast beastCopy = DDUtils.deepCopy(beast);
		
		heroCopy.performAttack(beast);
		return new HeroAndBeast(heroCopy, beastCopy);
	}
	@Override
	public void walkTo(Point point) throws IOException {
		if (!isReachable(point)) {
			return;
		}
		
		if (gameObjects.get(point).objectType == DDObjectType.PICKUP) {
			pickup((Pickup)gameObjects.get(point));
			return;
		}
		
		innerWalkTo(point);
		
		for (Point neigh : navigation.getNeighbors(point)) {
			if (gameObjects.get(neigh).objectType == DDObjectType.DARKNESS) {
				discoverAndHandle(neigh);
			}
		}
	}
	/**
	 * Issues a walk to a specific location, no matter what is there.
	 * @param point
	 * @throws IOException
	 */
	protected void innerWalkTo(Point point) throws IOException {
		gameObjects.moveHero(point);
	}
	@Override
	public void useGlyph(Glyph glyph) throws IOException {
		if (!glyph.isHeld())
			return;
		
		Hero hero = getHero();
		if (Hero.canSoloCast(glyph)) {
			hero.soloCast(glyph);
			return;
		}
		
		if (!hero.canCast(glyph))
			return;
		
		//int manaDiscount = hero.getManaDiscount();
		
		switch (glyph.getGlyphType()) {
			// All untargeted glyphs are currently solocastable,
			//so the control should never get here
		default:
			break;
		}
	}
	@Override
	public void useGlyph(Glyph glyph, DDObject target) throws IOException {
		if (!glyph.isHeld())
			return;
		
		Hero hero = getHero();
		if (!hero.canCast(glyph))
			return;
		
		switch (glyph.getGlyphType()) {
		case BURNDAYRAZ:
			Point move = neighborPoint(target.location);
			if (!(target instanceof Beast) || move.equals(new Point(-1, -1)))
				return;
			
			Beast beast = (Beast)target;
			
			if (!hero.performFireball(beast))
				return;
			
			walkTo(move);
			beastDealtDamage(beast);
			
			break;
		case APHEELSIK:
			move = neighborPoint(target.location);
			if (!(target instanceof Beast) || move.equals(new Point(-1, -1)))
				return;
			
			beast = (Beast)target;
			
			if (!hero.performPoison(beast))
				return;
			
			walkTo(move);
			break;
		default:
			break;
		}
	}

	@Override
	public void convertGlyph(Glyph glyph) throws IOException {
		getHero().convertGlyph(glyph);
	}

	@Override
	public void pickupGlyph(Glyph glyph) throws IOException {
		if (!isReachable(glyph)) {
			return;
		}
		walkTo(glyph.location);
		Hero hero = gameObjects.getHero();
		int freeSpot =  hero.getFreeGlyphSlot();
		if (freeSpot != -1) {
			gameObjects.underHero = new Empty(hero.location);
			glyph.location = new Point(-1, -1);
			hero.glyphs[freeSpot] = glyph;
		}
	}

	@Override
	public void pickup(Pickup pickup) throws IOException {
		if (!isReachable(pickup.location)) {
			return;
		}
		
		Hero hero = gameObjects.getHero();
		gameObjects.set(pickup.location, new Empty(pickup.location));
		
		if (pickup.getPickupType() == Type.ManaUp) {
			// Mana-up is collected after regeneration applies
			walkTo(pickup.location);
		}
		
		switch (pickup.getPickupType()) {
		case AttackUp:
			hero.percentAttackBonus += 10;
			break;
		case HealthUp:
			hero.percentHpBonus += 10;
			break;
		case ManaUp:
			hero.maxMana += 1;
			break;
		case HealthPotion:
			hero.hPotions += 1;
			break;
		case ManaPotion:
			hero.mPotions += 1;
			break;
		case Gold:
			hero.gainGold(1);
			break;
		default:
			break;
		}
		
		if (pickup.getPickupType() != Type.ManaUp) {
			// Everything except mana-up is collected before regeneration applies
			walkTo(pickup.location);
		}
	}

	@Override
	public void buyItem(Shop shop) throws IOException {
		
	}

	public void drinkHealthPotion() throws IOException {
		getHero().drinkHealthPotion();
	}
	public void drinkManaPotion() throws IOException {
		getHero().drinkManaPotion();
	}
	
	@Override
	public List<DDAction> getActions() {
		Hero hero = getHero();
		List<DDAction> ret = new ArrayList<DDAction>();
		for (Point move : getExplorationMoves()) {
			ret.add(new Walk(move));
		}
		for (Pickup pickup : gameObjects.getPickups()) {
			ret.add(new PickUpPickup(pickup));
		}
		for (Beast beast : gameObjects.getBeasts()) {
			if (navigation.isTouchable(beast.location)) {
				ret.add(new Attack(beast));
			}
		}
		for (Glyph glyph : gameObjects.getGlyphs()) {
			if (navigation.isReachable(glyph.location) && hero.hasFreeGlyphSlot()) {
				ret.add(new PickUpGlyph(glyph));
			}
		}
		for (Glyph glyph : hero.glyphs) {
			if (glyph == null)
				continue;
			if (glyph.getTargetType() == Glyph.Target.NONE) {
				ret.add(new UseGlyph(glyph));
			} else if (glyph.getTargetType() == Glyph.Target.BEAST) {
				for (Beast beast : gameObjects.getBeasts()) {
					if (navigation.isTouchable(beast.location)) {
						ret.add(new UseGlyph(glyph, beast));
					}
				} 
			}
			ret.add(new ConvertGlyph(glyph));
		}
		if (hero.hPotions > 0) {
			ret.add(new DrinkPotion(PotionKind.HEALTH));
		}
		if (hero.mPotions > 0) {
			ret.add(new DrinkPotion(PotionKind.MANA));
		}
		
		return ret;
	}

	@Override
	public void doAction(DDAction action) throws IOException {
		switch (action.actionType) {
		case WALK:
			walkTo(((Walk)action).point);
			break;
		case PICKUP:
			pickup(((PickUpPickup)action).pickup);
			break;
		case ATTACK:
			attack(((Attack)action).beast);
			break;
		case PICKUPGLYPH:
			pickupGlyph(((PickUpGlyph)action).glyph);
			break;
		case USEGLYPH:
			UseGlyph useGlyph = (UseGlyph)action;
			if (useGlyph.target == null) {
				useGlyph(useGlyph.glyph);
			} else {
				useGlyph(useGlyph.glyph, useGlyph.target);
			}
			break;
		case CONVERTGLYPH:
			convertGlyph(((ConvertGlyph)action).glyph);
			break;
		case DRINKPOTION:
			if (((DrinkPotion)action).potionKind == PotionKind.HEALTH) {
				drinkHealthPotion();
			} else {
				drinkManaPotion();
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void setCheatingVision(boolean value) throws IOException {
		cheatingVision = value;
	}

	@Override
	public boolean isBossSlain() {
		return bossSlain;
	}
	
	@Override
	public boolean isCheatingVision() {
		return cheatingVision;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof DetachedDDState)) {
			return false;
		}
		
		DetachedDDState other = (DetachedDDState)o;
		return other.gameObjects.equals(this.gameObjects);
	}

	@Override
	public String toString() {
		return gameObjects.toString();
	}
}
