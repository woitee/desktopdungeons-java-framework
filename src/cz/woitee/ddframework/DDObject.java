package cz.woitee.ddframework;

import java.awt.Point;
import java.io.Serializable;

/**
 * Common predecessor to all Desktop Dungeons in-game objects.
 * @author woitee
 *
 */
public abstract class DDObject implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;
	
	protected Point location;
	protected DDObjectType objectType;
	
	public DDObject (Point location) {
		this.location = location;
	}
	
	/**
	 * Returns the location of the object.
	 * If the location of the object isn't known, returns (-1, -1).
	 * @return (x,y) location of the object. 
	 */
	public Point getLocation() {
		return location;
	}
	
	void setLocation(Point location) {
		this.location = location;
	}
	
	/**
	 * Returns the type of the object.
	 * @return The type of the object.
	 */
	public DDObjectType getType() {
		return objectType;
	}
	
	/**
	 * Creates an object from string. Returns null if the string could not have been parsed.
	 * @param s String as found in the DD API specification, possibly 
	 * 			directly gained from Desktop Dungeons.
	 * @return
	 */
	static DDObject parse(String s, Point location) {
		DDObject ret = null;
		if ((ret = Darkness.parse(s, location)) != null ||
			(ret = Wall.parse(s, location)) != null ||
			(ret = Empty.parse(s, location)) != null ||
			(ret = Hero.parse(s, location)) != null ||
			(ret = Beast.parse(s, location)) != null ||
			(ret = Pickup.parse(s, location)) != null ||
			(ret = Glyph.parse(s, location)) != null ||
			(ret = Altar.parse(s, location)) != null ||
			(ret = Shop.parse(s, location)) != null) {
			
			return ret;
		}

		return null;
	}
	
	@Override
	public Object clone() {
		throw new RuntimeException("Cloning was not implemented for " + this.getClass());
	}
	
	/**
	 * Converts a dungeon object to a string, that can be send to API to set dungeon layout.
	 * @return
	 */
	public abstract String toLayoutString();
}
