package cz.woitee.ddframework;

import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A class that contains objects in game's state, and provides methods to access them.
 * 
 * @author woitee
 */
public class GameObjects implements Serializable {
	private static final long serialVersionUID = 1L;
	
	protected DDObject[][] grid;
	protected List<Altar> altars;
	protected List<Beast> beasts;
	protected List<Glyph> glyphs;
	protected List<Pickup> pickups;
	protected List<Shop> shops;
	protected Hero hero;
	protected DDObject underHero;
	
	public GameObjects(int width, int height) {
		grid = new DDObject[width][height];
		altars = new ArrayList<Altar>();
		beasts = new ArrayList<Beast>();
		glyphs = new ArrayList<Glyph>();
		pickups = new ArrayList<Pickup>();
		shops = new ArrayList<Shop>();
	}
	
	protected DDObject get(int x, int y) {
		return grid[x][y];
	}
	protected DDObject get(Point p) {
		return get(p.x, p.y);
	}
	
	public void set(int x, int y, DDObject value) {
		DDObject orig = grid[x][y];
		if (orig instanceof Altar) {
			altars.remove((Altar)orig);
		} else if (orig instanceof Beast) {
			beasts.remove((Beast)orig);
		} else if (orig instanceof Glyph) {
			glyphs.remove((Glyph)orig);
		} else if (orig instanceof Pickup) {
			pickups.remove((Pickup)orig);
		} else if (orig instanceof Shop) {
			shops.remove((Shop)orig);
		}
		
		value.location = new Point(x, y);
		grid[x][y] = value;
		
		if (value instanceof Hero) {
			hero = (Hero)value;
		} else if (value instanceof Altar) {
			altars.add((Altar)value);
		} else if (value instanceof Beast) {
			beasts.add((Beast)value);
		} else if (value instanceof Glyph) {
			glyphs.add((Glyph)value);
		} else if (value instanceof Pickup) {
			pickups.add((Pickup)value);
		} else if (value instanceof Shop) {
			shops.add((Shop)value);
		}
	}
	protected void set(Point point, DDObject value) {
		set(point.x, point.y, value);
	}
	
	public List<Altar> getAltars() {
		return altars;
	}
	public List<Beast> getBeasts() {
		return beasts;
	}
	public List<Glyph> getGlyphs() {
		return glyphs;
	}
	public List<Pickup> getPickups() {
		return pickups;
	}
	public List<Shop> getShops() {
		return shops;
	}
	public Hero getHero() {
		return hero;
	}
	public void moveHero(Point destination) {
		if (underHero == null) {
			underHero = new Empty(hero.location);
		}
		set(hero.location, underHero);
		underHero = get(destination);
		hero.location = destination;
		set(hero.location, hero);
	}
	
	public DDObject[][] getGridCopy() {
		return DDUtils.deepCopy(grid);
	}
	public GameObjects makeCopy() {
		return DDUtils.deepCopy(this);
	}
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof GameObjects)) {
			return false;
		}
		GameObjects other = (GameObjects)o;
		
		return Arrays.deepEquals(grid, other.grid);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int y = 0; y < grid[0].length; ++y) {
			for (int x = 0; x < grid.length; ++x) {
				sb.append(grid[x][y]);
				if (x > 0)
					sb.append(";");
			}
			sb.append("\n");
		}
		return sb.toString();
	}
}
