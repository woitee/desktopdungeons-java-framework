package cz.woitee.ddframework;

import java.awt.Point;
import java.io.Serializable;

/**
 * A DDObject representing an unrevealed field in the dungeon.
 * @author Vojtech Cerny
 *
 */
public class Darkness extends DDObject implements Serializable {
	private static final long serialVersionUID = 1L;

	public Darkness(Point location) {
		super(location);
		objectType = DDObjectType.DARKNESS;
	}
	
	/**
	 * Try parse a Darkness object, maybe sent from the API.
	 * @param s String to parse.
	 * @param location Location of the eventual Darkness object.
	 * @return A new Darkness object, or null, if the string didn't contain it.
	 */
	public static Darkness parse(String s, Point location) {
		if (s.equals("UNSEEN"))
			return new Darkness(location);
		return null;
	}
	
	@Override
	public Object clone() {
		return new Darkness((Point)location.clone());
	}
	
	@Override
	public String toString() {
		return "Darkness at " + location;
	}
	
	@Override
	public String toLayoutString() {
		return "#";
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Darkness)) {
			return false;
		}
		
		Darkness other = (Darkness)o;
		return  this.location.equals(other.location);
	}
}
