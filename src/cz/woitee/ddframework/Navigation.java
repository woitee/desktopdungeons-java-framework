package cz.woitee.ddframework;

import java.awt.Point;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * A class that maintains navigatable (reachable) and navigatable next-to (touchable)
 * locations of the game.
 * 
 * @author woitee
 */
public class Navigation {
	protected GameObjects gameObjects;
	protected boolean[][] reachables;
	protected boolean[][] touchables;
	protected static Point gameSize = new Point(20, 20);
	
	public Navigation(GameObjects gameObjects){
		this.gameObjects = gameObjects;
		reachables = new boolean[gameSize.x][gameSize.y];
		touchables = new boolean[gameSize.x][gameSize.y];
		calculateReachablesTouchables();
	}
	
	protected boolean isPassable(int x, int y) {
		DDObject obj = gameObjects.get(x, y);
		if (obj.getType() == DDObjectType.BEAST ||
			obj.getType() == DDObjectType.WALL ||
			obj.getType() == DDObjectType.DARKNESS) {
			
			return false;
		}
		return true;
	}
	protected boolean isPassable(Point point) {
		return isPassable(point.x, point.y);
	}
	public List<Point> getNeighbors(int x, int y) {
		List<Point> ret = new ArrayList<Point>();
		if (x > 0) {
			ret.add(new Point(x - 1, y));
			if (y > 0) {
				ret.add(new Point(x - 1, y - 1));
			}
			if (y < gameSize.y - 1) {
				ret.add(new Point(x - 1, y + 1));
			}
		}
		if (y > 0) {
			ret.add(new Point(x, y - 1));
		}
		if (y < gameSize.y - 1) {
			ret.add(new Point(x, y + 1));
		}
		if (x < gameSize.x - 1) {
			ret.add(new Point(x + 1, y));
			if (y > 0) {
				ret.add(new Point(x + 1, y - 1));
			}
			if (y < gameSize.y - 1) {
				ret.add(new Point(x + 1, y + 1));
			}
		}
		return ret;
	}
	public List<Point> getNeighbors(Point point) {
		return getNeighbors(point.x, point.y);
	}
	
	protected void calculateReachablesTouchables() {
		// BFS from hero's location
		Queue<Point> queue = new ArrayDeque<Point>();
		boolean[][] visited = new boolean[gameSize.x][gameSize.y];
		
		Point heroPos = gameObjects.getHero().location;
		queue.add(new Point(heroPos));
		visited[heroPos.x][heroPos.y] = true;
		reachables[heroPos.x][heroPos.y] = true;
		touchables[heroPos.x][heroPos.y] = true;
		
		while(!queue.isEmpty()) {
			Point cur = queue.poll();
			for (Point neigh : getNeighbors(cur)) {
				if (!visited[neigh.x][neigh.y]) {
					visited[neigh.x][neigh.y] = true;
					if (isPassable(neigh)) {
						reachables[neigh.x][neigh.y] = true;
						queue.add(neigh);
					}
					touchables[neigh.x][neigh.y] = true;
				}
			}
		}
	}
	
	public boolean isReachable(int x, int y) {
		return reachables[x][y];
	}
	public boolean isReachable(Point point) {
		return isReachable(point.x, point.y);
	}
	public boolean isTouchable(int x, int y) {
		return touchables[x][y];
	}
	public boolean isTouchable(Point point) {
		return isTouchable(point.x, point.y);
	}
	public void updateLocation(int x, int y) {
		if (isPassable(x,y)) {
			// The location is passable, we check if we can get to it
			for (Point neigh : getNeighbors(x, y)) {
				if (isReachable(neigh)) {
					reachables[x][y] = true;
					for (Point point : getNeighbors(x, y)) {
						touchables[point.x][point.y] = true;
					}
					return;
				}
			}
		}
		// If the new location isn't passable (i.e. a monster was summoned on it)
		// It could've completely changed the layout
		refreshAll();
	}
	public void updateLocation(Point point) {
		updateLocation(point.x, point.y);
	}
	public void refreshAll() {
		reachables = new boolean[gameSize.x][gameSize.y];
		touchables = new boolean[gameSize.x][gameSize.y];
		calculateReachablesTouchables();
	}
}
