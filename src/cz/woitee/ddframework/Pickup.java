package cz.woitee.ddframework;

import java.awt.Point;
import java.io.Serializable;

/**
 * A collectible pickup object in the game. Can be one of: Attack PowerUp,
 * Health PowerUp, Mana PowerUp, a Health Potion, a Mana Potion, Gold pile.
 * @author Vojtech Cerny
 *
 */
public class Pickup extends DDObject implements Serializable {
	private static final long serialVersionUID = 1L;
	private Type type;
	public Pickup(Type type, Point location) {
		super(location);
		this.type = type;
		objectType = DDObjectType.PICKUP;
	}
	/**
	 * Enumeration of pickup object types.
	 * @author Vojtech Cerny
	 *
	 */
	public enum Type {
		AttackUp("ATTACKUP"),
		HealthUp("HEALTHUP"),
		ManaUp("MANAUP"),
		HealthPotion("HPOTION"),
		ManaPotion("MPOTION"),
		Gold("GOLD");
		
		private String name;
		private Type(String name) {
			this.name = name;
		}
		
		/**
		 * Custom overriden, returns value ready to send over connection
		 * to Desktop Dungeons with API.
		 */
		@Override
		public String toString() {
			return name;
		}
	}
	public Type getPickupType() {
		return type;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Pickup)) {
			return false;
		}
		
		Pickup other = (Pickup)o;
		return  this.location.equals(other.location) &&
				this.type == other.type;
	}
	
	@Override
	public Object clone() {
		return new Pickup(type, (Point)location.clone());
	}
	
	@Override
	public String toString() {
		return type + " at " + location;
	}
	
	@Override
	public String toLayoutString() {
		if (type == Type.Gold)
			return "GOLD";
		else
			return "P_" + type.toString().substring(0, 3);
	}
	
	public static Pickup parse(String s, Point location) {
		String[] split = s.split(" ");
		if (split.length != 2 || !split[0].equals("PICKUP")) {
			return null;
		}
		for (Type type: Type.values()) {
			if (type.toString().equals(split[1])) {
				return new Pickup(type, location);
			}
		}
		return null;
	}
}
