package cz.woitee.ddframework;

import java.awt.Point;
import java.io.Serializable;

/**
 * A shop object in the game. Contains information about the item it sells,
 * and about it's cost.
 * @author Vojtech Cerny
 *
 */
public class Shop extends DDObject implements Serializable {
	private static final long serialVersionUID = 1L;
	private Item item;
	private int cost;
	public Shop(Item item, int cost, Point location) {
		super(location);
		objectType = DDObjectType.SHOP;
		this.item = item;
		this.cost = cost;
	}
	/**
	 * Enumeration of all game items.
	 * @author Vojtech Cerny
	 *
	 */
	public enum Item {
		PendantOfHealth("PENDANTHEALTH"),
		PendantOfMana("PENDANTMANA"),
		FineSword("FINESWORD"),
		HealthPotion("HPOTION"),
		ManaPotion("MPOTION"),
		BloodySigil("BLOODYSIGIL"),
		ViperWard("VIPERWARD"),
		SoulOrb("SOULDORB"),
		TrollHeart("TROLLHEART"),
		TowerShield("TOWERSHIELD"),
		MageHelm("MAGEHELM"),
		ScoutingOrb("SCOUTINGORB"),
		BlueBead("BLUEBEAD"),
		StoneOfSeekers("STONEOFSEEKERS"),
		Spoon("SPOON"),
		StoneSigil("STONESIGIL"),
		BadgeOfCourage("BADGEOFCOURAGE"),
		TalismanOfRebirth("TALISMANOFREBIRTH"),
		SignOfTheSpirits("SIGNOFTHESPIRITS"),
		BoneBreaker("BONEBREAKER"),
		StoneHeart("STONEHEART"),
		FireHeart("FIREHEART"),
		Platemail("PLATEMAIL"),
		MagePlate("MAGEPLATE"),
		VenomBlade("VENOMBLADE"),
		FlamingSword("FLAMINGSWORD"),
		DancingSword("DANCINGSWORD"),
		ZombieDog("ZOMBIEDOG"),
		DwarvenGauntlets("DWARVENGAUNTLETS"),
		ElvenBoots("ELVENBOOTS"),
		KegOHealth("KEGOHEALTH"),
		KegOMagic("KEGOMAGIC"),
		WeytwutGlyph("WEYTWUT"),
		HalpmehGlyph("HALPMEH"),
		BludtupowaGlyph("BLUDTUPOWA"),
		CrystalBall("CRYSTALBALL"),
		AgnosticsCollar("AGNOSTICSCOLLAR"),
		VampiricSword("VAMPIRICSWORD"),
		SpikedFlail("SPIKEDFLAIL"),
		AlchemistsScroll("ALCHEMISTSSCROLL"),
		RingOgTheBattlemage("RINGOFTHEBMAGE"),
		WickedGuitar("WICKEDGUITAR"),
		BerserkersBlade("BERSERKERSBLADE"),
		MagiciansMoonstrike("MOONSTRIKE"),
		TerrorSlice("TERRORSLICE"),
		AmuletOfYendor("AMULETOFYENDOR"),
		OrbOfZot("ORBOFZOT");
		
		private String name;
		private Item(String name) {
			this.name = name;
		}
		
		/**
		 * Custom overriden, returns value ready to send over connection
		 * to Desktop Dungeons with API.
		 */
		@Override
		public String toString() {
			return name;
		}
	}
	public Item getItem() {
		return item;
	}
	public int getCost() {
		return cost;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Shop)) {
			return false;
		}
		
		Shop other = (Shop)o;
		return  this.location.equals(other.location) &&
				this.getItem() == other.getItem() &&
				this.cost == other.cost;
	}
	
	@Override
	public Object clone() {
		return new Shop(item, cost, (Point)location.clone());
	}
	
	@Override
	public String toString() {
		return "Shop selling " + item + " for " + cost;
	}
	
	@Override
	public String toLayoutString() {
		return "S_" + item.ordinal();
	}
	
	public static Shop parse (String s, Point location) {
		String[] split = s.split(" ");
		if (split.length != 3 || !split[0].equals("SHOP")) {
			return null;
		}
		Item item = null;
		for (Item it: Item.values()) {
			if (it.toString().equals(split[1])) {
				item = it;
			}
		}
		if (item == null)
			return null;
		try {
			int cost = Integer.parseInt(split[2]);
			return new Shop(item, cost, location);
		} catch (NumberFormatException e) {
			return null;
		}
	}
}
