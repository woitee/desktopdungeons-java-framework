package cz.woitee.ddframework;

import java.awt.Point;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * The hero of the game. This object holds information about the hero,
 * and also allows to perform some non-dungeon-changing actions, like attacks
 * and spell simulations. Those should be however performed only in detached
 * states, or hero clones, as it doesn't keep the state consistent.
 * 
 * @author Vojtech Cerny
 *
 */
public class Hero extends Beast implements Serializable {
	private static final long serialVersionUID = 1L;
	
	// For use in sets
	protected static class BeastInfo implements Serializable {
		private static final long serialVersionUID = 1L;
		
		public Beast.Type type;
		public Point location;
		public int level;
		
		public BeastInfo(Beast beast) {
			type = beast.type;
			location = beast.location;
			level = beast.level;
		}
		
		@Override
		public boolean equals(Object o) {
			if (!(o instanceof BeastInfo)) {
				return false;
			}
			
			BeastInfo other = (BeastInfo) o;
			return  other.type.equals(this.type) &&
					other.location.equals(this.location) &&
					other.level == this.level;
		}
		
		@Override
		public String toString() {
			return String.format("BeastInfo of %1s lvl%2d at %3", type, level, location);
		}
		
		@Override
		public int hashCode() {
			int result = type.ordinal();
			result = 37 * result + 57;
			result += location.hashCode();
			result = 37 * result + 57;
			result += level;
			
			return result;
		}
	}
	
	public Hero(Point location) {
		super(Type.Hero, 1, 5, 5, 5, location);
		objectType = DDObjectType.HERO;
	}
	
	private void adjustCustomClass() {
		switch (clazz) {
			case Fighter:
				extraXp = 1;
				killProtect = true;
				break;
			case Priest:
				hpIncrease = 12;
				break;
			case Berserker:
				magicResist = 50;
				break;
			case Monk:
				magicResist = 50;
				physicResist = 50;
				break;
			default:
				break;
		}
	}
	
	/**
	 * Enumeration of hero races.
	 * @author Vojtech Cerny
	 *
	 */
	public enum Race {
		Human("HUMAN"),
		Elf("ELF"),
		Dwarf("DWARF"),
		Halfling("HALFLING"),
		Gnome("GNOME"),
		Goblin("GOBLIN"),
		Orc("ORC");
		
		private String name;
		private Race(String name) {
			this.name = name;
		}
		/**
		 * Custom overriden, returns value ready to send over connection
		 * to Desktop Dungeons with API.
		 */
		@Override
		public String toString() {
			return name;
		}
		
		/**
		 * Parses a race value from string.
		 * Return null if not possible to parse.
		 * @param s The string to parse.
		 * @return
		 */
		public static Race parse(String s) {
			for (Race race : Race.values()) {
				if (race.toString().equals(s)) {
					return race;
				}
			}
			return null;
		}
	}
	/**
	 * Enumeration of hero classes.
	 * @author Vojtech Cerny
	 *
	 */
	public enum Clazz {
		Fighter("FIGHTER"),
		Thief("THIEF"),
		Priest("PRIEST"),
		Wizard("WIZARD"),
		Berserker("BERSERKER"),
		Rogue("ROGUE"),
		Monk("MONK"),
		Sorcerer("SORCERER"),
		Warlord("WARLORD"),
		Assassin("ASSASSIN"),
		Paladin("PALADIN"),
		Bloodmage("BLOODMAGE"),
		Transmuter("TRANSMUTER"),
		Crusader("CRUSADER"),
		Tinker("TINKER");
		
		private String name;
		private Clazz(String name) {
			this.name = name;
		}
		/**
		 * Custom overriden, returns value ready to send over connection
		 * to Desktop Dungeons with API.
		 */
		@Override
		public String toString() {
			return name;
		}
		
		/**
		 * Parses a clazz from a string.
		 * Returns null if not possible to parse.
		 * @param s String to parse.
		 * @return
		 */
		public static Clazz parse(String s) {
			for (Clazz clazz : Clazz.values()) {
				if (clazz.toString().equals(s)) {
					return clazz;
				}
			}
			return null;
		}
	}
	
	// Common stats
	Race race;
	Clazz clazz;
	int xp;
	int maxHpBase;
	int percentHpBonus = 0;
	int absHpBonus = 0;
	int percentAttackBonus;
	int oneShotPercentAttackBonus = 0;
	int mana;
	int maxMana;
	int gold;
	Altar.God god;
	int piety;
	int hPotions;
	int mPotions;
	Glyph[] glyphs;
	boolean manaBurned;
	boolean killProtect;
	boolean oneShotFirstStrike;
	// Just for Thief
	Set<BeastInfo> attacked = new HashSet<BeastInfo>();
	
	// Uncommon stats
	protected int hpIncrease = 10;
	protected int attackIncrease = 5;
	protected int mpIncrease = 0;
	protected int extraXp = 0;
	public boolean firstStrike = false;

	void getAdditionalInfoFrom(ICommunicator comm) throws IOException {
		comm.send("HERORACECLASS");
		String s = comm.receive();
		String[] split = s.split(" ");
		race = Race.parse(split[0]);
		clazz = Clazz.parse(split[1]);
		
		comm.send("HEROSTATE");
		s = comm.receive();
		split = s.split(" ");
		level = Integer.parseInt(split[0]);
		attackBase = Integer.parseInt(split[1]);
		percentAttackBonus = Integer.parseInt(split[2]);
		xp = Integer.parseInt(split[3]);
		health = Integer.parseInt(split[4]);
		maxHpBase = Integer.parseInt(split[5]);
		poisoned = split[6].equals("TRUE") ? true : false;
		mana = Integer.parseInt(split[7]);
		maxMana = Integer.parseInt(split[8]);
		gold = Integer.parseInt(split[9]);
		god = Altar.God.parse(split[10]);
		piety = Integer.parseInt(split[11]);
		
		comm.send("HEROITEMS");
		s = comm.receive();
		split = s.split(" ");
		hPotions = Integer.parseInt(split[0]);
		mPotions = Integer.parseInt(split[1]);
		glyphs = new Glyph[split.length - 2];
		for (int i = 0; i < split.length - 2; ++i) {
			if (split[i+2].equals("NONE")) {
				glyphs[i] = null;
			} else {
				glyphs[i] = Glyph.parse("GLYPH " + split[i+2]);
			}
		}
		
		adjustCustomClass();
	}
	
	public int getSlotOf(Glyph glyph) {
		for (int i = 0; i < glyphs.length; ++i) {
			if (glyph.equals(glyphs[i])) {
				return i;
			}
		}
		return -1;
	}
	public boolean isDead() {
		return health <= 0;
	}
	
	@Override
	void dealDamageTo(Beast beast) {
		if (clazz == Clazz.Thief && !attacked.contains(new BeastInfo(beast))) {
			oneShotPercentAttackBonus += 30;
			attacked.add(new BeastInfo(beast));
		}
		if (clazz == Clazz.Berserker && beast.level > this.level) {
			oneShotPercentAttackBonus += 30;
		}
		
		int thisAttackDmg = getAttack();
		
		if (clazz == Clazz.Priest && beast.isUndead())
			thisAttackDmg *= 2;
		
		dealDamageTo(beast, thisAttackDmg, false);
		oneShotPercentAttackBonus = 0;
		oneShotFirstStrike = false;
	}
	void dealDamageTo(Beast beast, int damage, boolean isMagical) {
		beast.gainHealth(-Beast.getResistedAttack(beast, isMagical, damage));
	}
	@Override
	public int damageTo(Beast beast) {
		if (clazz == Clazz.Thief && !attacked.contains(new BeastInfo(beast))) {
			oneShotPercentAttackBonus += 30;
		}
		if (clazz == Clazz.Berserker && beast.level > this.level) {
			oneShotPercentAttackBonus += 30;
		}
		
		int thisAttackDmg = getAttack();
		
		if (clazz == Clazz.Thief && !attacked.contains(new BeastInfo(beast))) {
			oneShotPercentAttackBonus -= 30;
		}
		
		if (clazz == Clazz.Priest && beast.isUndead())
			thisAttackDmg *= 2;
		
		return Beast.getResistedAttack(beast, false, thisAttackDmg);
	}
	
	/**
	 * Returns whether this hero's attack is done before the beast's.
	 * @param beast
	 * @return
	 */
	public boolean fightsBefore(Beast beast) {
		if (beast.hasFirstStrike()) {
			return false;
		}
		return hasFirstStrike() || this.level > beast.level;
	}
	
	@Override
	void gainHealth(int amount) {
		health = DDUtils.clamp(health + amount, 0, getMaxHealth());
	}
	void receiveDmg(Beast source, int amount) {
		health -= amount;
		if (health <= 0 && killProtect) {
			health = 1;
			killProtect = false;
		}
		
		if (clazz == Clazz.Sorcerer) {
			dealDamageTo(source, this.level, true);
		}
	}
	void gainMana(int amount) {
		mana = DDUtils.clamp(mana + amount, 0, maxMana);
		if (amount < 0 && clazz == Clazz.Sorcerer) {
			gainHealth(-2 * amount);
		}
	}
	void gainGold(int amount) {
		gold = DDUtils.clamp(gold + amount, 0, 70);
	}
	void gainXp(int amount) {
		xp += amount;
		while (xp >= level * 5) {
			xp -= level * 5;
			levelUp();
		}
	}
	void gainXp(Beast killed) {
		int levelDiff = killed.level - this.level;
		int xp = killed.level;
		if (levelDiff > 0) {
			xp += levelDiff * (levelDiff + 1);
		}
		xp += extraXp;
		
		gainXp(xp);
	}
	void levelUp() {
		level += 1;
		maxHpBase += hpIncrease;
		health = getMaxHealth();
		maxMana += mpIncrease;
		mana = maxMana;
		attackBase += attackIncrease;
		poisoned = false;
		manaBurned = false;
	}
	void gainConversionBonus() {
		switch (race) {
			case Human:
				percentAttackBonus += 10;
				break;
			case Elf:
				maxMana += 2;
				break;
			case Dwarf:
				percentHpBonus += 10;
				break;
			case Halfling:
				hPotions += 1;
				break;
			case Gnome:
				mPotions += 1;
				break;
			case Orc:
				attackBase += 2;
			case Goblin:
				gainXp(5);
			default:
				break;
		}
	}
	
	
	/**
	 * Attacks a monster, should be only used with detached hero's,
	 * as it doesn't keep state consistent.
	 * @param beast
	 */
	public void performAttack(Beast beast) {
		if (this.fightsBefore(beast)) {
			this.dealDamageTo(beast);
			if (!beast.isDead()) {
				beast.dealDamageTo(this);
			}
		} else {
			beast.dealDamageTo(this);
			this.dealDamageTo(beast);
		}
		
		if (beast.isDead()) {
			if (!this.isDead())
				this.gainXp(beast);
		}
	}
	/**
	 * Poisons a monster, should be only used with detached hero's,
	 * as it doesn't keep state consistent.
	 * 
	 * Returns whether the hero was able to cast poison.
	 * @param beast
	 */
	public boolean performPoison(Beast beast) {
		Glyph myPoison = this.getGlyphOfType(Glyph.Type.APHEELSIK);
		if (myPoison == null || !canCast(myPoison))
			return false;
		
		this.gainMana(-5 + this.getManaDiscount());
		if (!beast.isUndead()) {
			beast.poisoned = true;
		}
		
		return true;
	}
	/**
	 * Fireballs a monster, should be only used with detached hero's,
	 * as it doesn't keep state consistent.
	 * 
	 * Returns whether the hero was able to cast the fireball.
	 * @param beast
	 */
	public boolean performFireball(Beast beast) {
		Glyph myFireball = this.getGlyphOfType(Glyph.Type.BURNDAYRAZ);
		if (myFireball == null || !canCast(myFireball))
			return false;
		
		this.gainMana(-6 + this.getManaDiscount());
		int damage = Beast.getResistedAttack(beast, true, this.level * 4);
		beast.gainHealth(-damage);
		
		if (beast.isDead()) {
			if (!this.isDead())
				this.gainXp(beast);
		}
		
		return true;
	}
	public void convertGlyph(Glyph glyph) {
		if (!glyph.isHeld())
			return;
		
		int slot = getSlotOf(glyph);
		if (slot == -1)
			return;
		
		glyphs[slot] = null;
		gainConversionBonus();
	}
	// CASTING GLYPHS
	public static boolean canSoloCast(Glyph glyph) {
		return glyph.getGlyphType() == Glyph.Type.BYSSEPS ||
				glyph.getGlyphType() == Glyph.Type.GETINDARE ||
				glyph.getGlyphType() == Glyph.Type.CYDSTEPP;
	}
	public void soloCast(Glyph glyph) {
		switch (glyph.getGlyphType()) {
		case BYSSEPS:
			oneShotPercentAttackBonus = 30;
			gainMana(-2 + getManaDiscount());
			break;
		case GETINDARE:
			oneShotFirstStrike = true;
			gainMana(-3 + getManaDiscount());
			break;
		case CYDSTEPP:
			killProtect = true;
			gainMana(-10 + getManaDiscount());
		default:
			break;
		}
	}
	public void drinkHealthPotion() {
		if (hPotions <= 0)
			return;
		
		--hPotions;
		poisoned = false;
		if (clazz == Hero.Clazz.Priest)
			gainHealth(getMaxHealth());
		else
			gainHealth(DDUtils.percentageOf(getMaxHealth(), 40));
		
		if (clazz == Hero.Clazz.Thief) {
			gainMana(DDUtils.percentageOf(getMaxMana(), 40));
		}
	}
	public void drinkManaPotion() {
		if (mPotions <= 0)
			return;
		
		--mPotions;
		manaBurned = false;
		gainMana(DDUtils.percentageOf(getMaxMana(), 40));
		if (clazz == Hero.Clazz.Thief) {
			gainHealth(DDUtils.percentageOf(getMaxHealth(), 40));
		}
	}
	
	public Race getRace() {
		return race;
	}
	public Clazz getClazz() {
		return clazz;
	}
	public int getLevel() {
		return level;
	}
	public int getXP() {
		return xp;
	}
	@Override
	public int getAttack() {
		int base = super.getAttack();
		return addPercentage(base, percentAttackBonus + oneShotPercentAttackBonus);
	}
	@Override
	public boolean hasFirstStrike() {
		return firstStrike || oneShotFirstStrike;
	}
	public boolean hasKillProtect() {
		return killProtect;
	}
	public int getHealth() {
		return health;
	}
	public int getMaxHealth() {
		return absHpBonus + addPercentage(maxHpBase, percentHpBonus);
	}
	public int getMana() {
		return mana;
	}
	public int getMaxMana() {
		return maxMana;
	}
	public int getGold() {
		return gold;
	}
	public Altar.God getGod() {
		return god;
	}
	public int getPiety() {
		return piety;
	}
	public int getHPotions() {
		return hPotions;
	}
	public int getMPotions() {
		return mPotions;
	}
	public Glyph getGlyph(int index) {
		return glyphs[index];
	}
	public Glyph[] getGlyphs() {
		return Arrays.copyOf(glyphs, glyphs.length);
	}
	public Glyph getGlyphOfType(Glyph.Type glyphType) {
		for (Glyph glyph : glyphs) {
			if (glyph != null && glyph.getGlyphType() == glyphType) {
				return glyph;
			}
		}
		return null;
	}
	public int getOneShotPercentAttackBonus() {
		return oneShotPercentAttackBonus;
	}
	public boolean isManaBurned() {
		return manaBurned;
	}
	public int getManaDiscount() {
		switch (clazz) {
		case Wizard:
			return 1;
		case Berserker:
			return -2;
		default:
			return 0;
		}
	}
	public boolean canCast(Glyph glyph) {
		int manaDiscount = getManaDiscount();
		
		switch (glyph.getGlyphType()) {
		case BURNDAYRAZ:
			return mana >= 6 - manaDiscount;
		case BYSSEPS:
			return mana >= 2 - manaDiscount;
		case GETINDARE:
			return mana >= 3 - manaDiscount;
		case CYDSTEPP:
			return mana >= 10 - manaDiscount;
		case APHEELSIK:
			return mana >= 5 - manaDiscount;
		default:
			System.err.println("Unimplemented glyph cost: " + glyph);
			return false;
		}
	}
	public int getFreeGlyphSlot() {
		for (int i = 0; i < glyphs.length; ++i) {
			if (glyphs[i] == null) {
				return i;
			}
		}
		return -1;
	}
	public boolean hasFreeGlyphSlot() {
		return getFreeGlyphSlot() != -1;
	}
	
	@Override
	public Object clone() {
		Hero clone = new Hero((Point)location.clone());
		//firstly copy data from parent class (Beast)
		clone.health = health;
		clone.attackBase = attackBase;
		clone.level = level;
		
		//then actual Hero data
		clone.race = race;
		clone.clazz = clazz;
		clone.xp = xp;
		clone.maxHpBase = maxHpBase;
		clone.percentHpBonus = percentHpBonus;
		clone.absHpBonus = absHpBonus;
		clone.percentAttackBonus = percentAttackBonus;
		clone.oneShotPercentAttackBonus = oneShotPercentAttackBonus;
		clone.mana = mana;
		clone.maxMana = maxMana;
		clone.gold = gold;
		clone.piety = piety;
		clone.hPotions = hPotions;
		clone.mPotions = mPotions;
		
		clone.glyphs = new Glyph[glyphs.length];
		for (int i = 0; i < glyphs.length; ++i) {
			if (glyphs[i] != null) {
				clone.glyphs[i] = (Glyph)glyphs[i].clone();
			}
		}
		
		clone.manaBurned = manaBurned;
		clone.killProtect = killProtect;
		clone.oneShotFirstStrike = oneShotFirstStrike;
		if (clazz == Clazz.Thief) {
			clone.attacked = new HashSet<BeastInfo>();
			for (BeastInfo bInfo : attacked) {
				clone.attacked.add(bInfo);
			}
		}
		
		clone.hpIncrease = hpIncrease;
		clone.attackIncrease = attackIncrease;
		clone.mpIncrease = mpIncrease;
		clone.extraXp = extraXp;
		clone.firstStrike = firstStrike;
		
		return clone;
	}
	
	@Override
	public String toString() {
		String glyphsString = "";
		for (Glyph glyph : glyphs) {
			if (glyph == null)
				glyphsString += "NONE ";
			else
				glyphsString += glyph.getGlyphType() + " ";
		}
		
		return "Hero " + getRace() + " " + getClazz() +
				" lvl:" + getLevel() +
				" att:" + getAttack() +
				" hp:" + getHealth() + "/" + getMaxHealth() +
				" mp:" + getMana() + "/" + maxMana +
				" xp:" + getXP() +
				" gold:" + getGold() +
				" god:" + getGod() + " piety:" + getPiety() +
				" hPotions:" + getHPotions() +
				" mPotions:" + getMPotions() +
				" poisoned:" + isPoisoned() +
				" kill_protect:" + killProtect +
				" glyphs:" + glyphsString +
				"at " + location;
	}
	
	@Override
	public String toLayoutString() {
		return "HERO";
	}
	
	public static Hero parse (String s, Point location) {
		if (s.equals("HERO")) {
			return new Hero(location);
		}
		
		return null;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Hero)) {
			return false;
		}
		
		Hero other = (Hero)o;
		return  this.location.equals(other.location) &&
				this.getRace() == other.getRace() &&
				this.getClass() == other.getClass() &&
				this.getHealth() == other.getHealth() &&
				this.getMaxHealth() == other.getMaxHealth() &&
				this.getMana() == other.getMana() &&
				this.getMaxMana() == other.getMaxMana() &&
				this.getLevel() == other.getLevel() &&
				this.getXP() == other.getXP() &&
				this.isPoisoned() == other.isPoisoned() &&
				this.getGod() == other.getGod() &&
				//Not checking for equality of gold, because getting gold from pile is random
				//this.getGold() == other.getGold() &&
				this.getPiety() == other.getPiety() &&
				this.getAttack() == other.getAttack() &&
				Arrays.equals(glyphs, other.glyphs);
	}
}
