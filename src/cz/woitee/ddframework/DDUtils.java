package cz.woitee.ddframework;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.Random;

class DDUtils {
	private static Random rng = new Random();
	
	@SuppressWarnings("unchecked")
	public static <T> T deepCopy(T object) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(object);
	
			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bais);
			return (T)ois.readObject();
		} catch (IOException e) {
			return null;
		} catch (ClassNotFoundException e) {
			return null;
		}
	}
	
	public static <T extends Comparable<T>> T clamp(T amount, T low, T high) {
		if (amount.compareTo(low) < 0) {
			return low;
		} else if (amount.compareTo(high) > 0) {
			return high;
		}
		return amount;
	}
	
	static int percentageOf(int value, int percentage) {
		double percent = percentage / 100.0;
		return (int) Math.floor(value * percent);
	}
	public static void setRandomSeed(long value) {
		rng.setSeed(value);
	}
	public static <T> T getRandom(List<T> list) {
		return list.get(rng.nextInt(list.size()));
	}
	public static <T> T getRandom(T[] array) {
		return array[rng.nextInt(array.length)];
	}
	public static Random getRNG() {
		return rng;
	}
}
