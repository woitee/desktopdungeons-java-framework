package cz.woitee.ddframework;

import java.io.Serializable;

/**
 * A Boon (bonus) of one of the gods.
 * @author Vojtech Cerny
 *
 */
// this class is unfinished
public class Boon implements Serializable {
	private static final long serialVersionUID = 1L;
	enum Type {
		TODO
	}
	public Type getBoonType() {
		return Type.TODO;
	}
	public Altar.God getGodType() {
		return Altar.God.Earthmother;
	}
	public int getPietyCost() {
		return 0;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Boon)) {
			return false;
		}
		
		Boon other = (Boon)o;
		return  this.getBoonType() == other.getBoonType();
	}
}
