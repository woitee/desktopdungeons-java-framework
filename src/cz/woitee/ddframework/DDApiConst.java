package cz.woitee.ddframework;

/**
 * Constants used in the Desktop Dungeons API project.
 * @author Vojtech Cerny
 *
 */
public class DDApiConst {
	public static final boolean debug = false;
}
