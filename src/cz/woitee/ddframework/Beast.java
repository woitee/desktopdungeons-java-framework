package cz.woitee.ddframework;

import java.awt.Point;
import java.io.Serializable;

import cz.woitee.ddframework.Hero;

/**
 * A beast (or monster) object in the game.
 * Hero is considered a special type of beast.
 * @author Vojtech Cerny
 *
 */
public class Beast extends DDObject implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * Describes the type of the monster.
	 * @author Vojtech Cerny
	 *
	 */
	public enum Type {
		AnimatedArmor("ANIMARMOR"),
		Bandit("BANDIT"),
		Dragonspawn("DRAGONSPAWN"),
		Goat("GOAT"),
		Goblin("GOBLIN"),
		Golem("GOLEM"),
		Goo("GOO"),
		Gorgon("GORGON"),
		Hero("HERO"),
		Imp("IMP"),
		Meatman("MEATMAN"),
		Naga("NAGA"),
		Plant("PLANT"),
		Serpent("SERPENT"),
		Vampire("VAMPIRE"),
		Warlock("WARLOCK"),
		Wraith("WRAITH"),
		Zombie("ZOMBIE");
		
		private String name;
		private Type(String name) {
			this.name = name;
		}
		
		/**
		 * Custom overriden, returns value ready to send over connection
		 * to Desktop Dungeons with API.
		 */
		@Override
		public String toString() {
			return name;
		}
	}
	
	public Type type;
	int level, attackBase, health;
	int magicResist; int physicResist;
	private int maxHealth;
	boolean poisoned;
	
	/**
	 * Constructs a beast with all values fully set.
	 * @param type Type of the beast.
	 * @param level Level of the beast.
	 * @param attack Attack damage of the beast.
	 * @param health Health of the beast.
	 * @param maxHealth Maximum health of the beast.
	 * @param location Location of the beast.
	 */
	public Beast(Type type, int level, int attack, int health, int maxHealth, Point location) {
		super(location);
		objectType = DDObjectType.BEAST;
		this.type = type;
		this.level = level;
		this.attackBase = attack;
		this.health = health;
		this.maxHealth = maxHealth;
		
		switch (type) {
		case Goat:
			if (level < 10)
				magicResist = 25;
			else
				magicResist = 60;
			break;
		case Goblin:
			if (level == 10) {
				physicResist = 20;
				magicResist = 20;
			}	
			break;
		case Wraith:
			if (level < 10)
				physicResist = 30;
			else
				physicResist = 60;
			break;
		case Golem:
			if (level < 10)
				magicResist = 50;
			else
				magicResist = 75;
			break;
		case Goo:
			if (level < 10)
				physicResist = 50;
			else
				physicResist = 75;
			break;	
		default:
			break;
		}
	}
	
	public int getLevel() {
		return level;
	}
	public int getAttack() {
		return attackBase;
	}
	public int getHealth() {
		return health;
	}
	public int getMaxHealth() {
		return maxHealth;
	}
	public boolean isPoisonable() {
		return !isUndead();
	}
	public boolean isPoisoned() {
		return poisoned;
	}
	public boolean hasFirstStrike() {
		return type == Type.Goblin || type == Type.Gorgon;
	}
	public boolean isUndead() {
		return type == Type.Vampire || type == Type.Wraith || type == Type.Zombie;
	}
	public boolean hasMagicalAttack() {
		return type == Type.Dragonspawn || type == Type.Warlock || type == Type.Wraith;
	}
	void gainHealth(int amount) {
		health = DDUtils.clamp(health + amount, 0, getMaxHealth());
	}
	public int getMagicResist() {
		return magicResist;
	}
	public int getPhysicalResist() {
		return physicResist;
	}
	
	/**
	 * Deals damage to a hero, and applies all special effect of the attack.
	 * @param monster A hero to deal damage to.
	 */
	void dealDamageTo(Beast monster) {
		Hero hero = (Hero)monster;
		int attackDmg = damageTo(hero);
		
		if (this.type == Type.Serpent) {
			hero.poisoned = true;
		}
		
		if (this.type == Type.Wraith) {
			hero.mana = 0;
			hero.manaBurned = true;
		}
		
		hero.receiveDmg(this, attackDmg);
	}
	/**
	 * Returns a calculation of how much damage would the beast deal to hero on
	 * next attack.
	 * @param hero Hero to deal damage to.
	 * @return
	 */
	public int damageTo(Beast hero) {
		int attackDmg = getResistedAttack(hero, hasMagicalAttack(), getAttack());
		
		if (this.type == Type.Gorgon) {
			double epsilon = 0.01;
			if ((this.level < 10 && hero.getHealth() < hero.getMaxHealth() / 2.0 + epsilon) ||
				(this.level == 10 && hero.getHealth() < hero.getMaxHealth())) {
				attackDmg = Integer.MAX_VALUE;
			}
		}
		
		return attackDmg;
	}
	
	public boolean isDead() {
		return health <= 0;
	}
	
	protected static int addPercentage(int base, int percentage) {
		double epsilon = 0.0001;
		double percent = (100 + percentage) / 100.0;
		return (int) Math.floor(base * percent + epsilon);
	}
	
	/**
	 * Returns a correct calculation of how attack is resisted.
	 * @param target Beast (or hero) resisting the attack.
	 * @param isMagical Whether the attack is magical.
	 * @param attackDmg Damage the attack causes defaultly.
	 * @return Resisted damage of the attack.
	 */
	public static int getResistedAttack(Beast target, boolean isMagical, int attackDmg) {
		if (isMagical) {
			return addPercentage(attackDmg, -target.magicResist);
		} else {
			return addPercentage(attackDmg, -target.physicResist);
		}
	}
	
	@Override
	public Object clone() {
		Beast clone = new Beast(type, level, attackBase, health, maxHealth, (Point)location.clone());
		clone.poisoned = poisoned;
		return clone;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Beast)) {
			return false;
		}
		
		Beast other = (Beast)o;
		return  this.location.equals(other.location) &&
				this.type == other.type &&
				this.health == other.health &&
				this.maxHealth == other.maxHealth &&
				this.level == other.level;
	}
	
	@Override
	public String toString() {
		return  "Beast " + type +
				" lvl:" + level +
				" att:" + attackBase +
				" hp:" + health + "/" + maxHealth +
				" at " + location;
	}
	
	@Override
	public String toLayoutString() {
		return type.toString().substring(0, 3) + "_" + level;
	}
	
	/**
	 * Creates a Beast instance from string, possibly gained from Desktop Dungeons through API.
	 * @param s
	 * @return
	 */
	public static Beast parse(String s, Point location) {
		String[] split = s.split(" ");
		if (split.length != 6) {
			return null;
		}
		if (split[0].equals("BEAST")) {
			Type type = null;
			for (Type t: Type.values()) {
				if (t.toString().equals(split[1])) {
					type = t;
				}
			}
			if (type == null) { return null; }
			
			int level, attack, health, maxHealth;
			try {
				level = Integer.parseInt(split[2]);
				attack = Integer.parseInt(split[3]);
				health = Integer.parseInt(split[4]);
				maxHealth = Integer.parseInt(split[5]);
			} catch (NumberFormatException e) {
				return null;
			}
			
			return new Beast(type, level, attack, health, maxHealth, location);
		}
		return null;
	}
}
