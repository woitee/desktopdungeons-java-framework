package cz.woitee.ddframework;

import java.awt.Container;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

/**
 * Class providing an easy way to create a custom bot for Desktop Dungeons.
 * Simply inheriting from this class and overriding its {@link #step()} method with
 * custom behaviour is enough to create a bot.
 * 
 * To end the current run and start a new one, simply set the property "finished" to true.
 * 
 * You can run the resulting bot by calling {@link #run()}, or {@link #loop()} methods.
 * 
 * @author Vojtech Cerny
 *
 */
public abstract class BotBase {
	/**
	 * The api object of the game.
	 */
	protected DDApi api;
	/**
	 * The state object of the game.
	 */
	protected DDState state;
	/**
	 * Interval between bots steps, in milliseconds.
	 */
	protected long millisBetween = 200;
	/**
	 * Wait time after each run of the bot, in milliseconds.
	 */
	protected long millisBetweenRuns = 2000;
	/**
	 * Whether the bot is finished.
	 */
	protected boolean finished;
	/**
	 * Amount of runs the bot has already completed.
	 */
	private int runs = 0;
	
	/**
	 * Name of the bot. Used in minimal GUI frame when run in a loop.
	 */
	public String name = "BaseBot";
	
	// Getters / Setters
	public int getRuns() {
		return runs;
	}
	
	/**
	 * Set to change the class of the hero.
	 */
	protected Hero.Clazz heroClass = Hero.Clazz.Fighter;
	/**
	 * Set to change the race of the hero.
	 */
	protected Hero.Race heroRace = Hero.Race.Human;
	/**
	 * Set to change the game's type.
	 */
	protected GameType gameType = GameType.Normal;
	/**
	 * Set if you want to have cheating vision.
	 */
	protected boolean cheatingVision = false;
	private List<Beast.Type> customBeasts = null;
	
	public BotBase() throws IOException {
		api = new DDApi();
	}
	
	static <T extends Comparable<T>> T clamp(T amount, T low, T high) {
		if (amount.compareTo(low) < 0) {
			return low;
		} else if (amount.compareTo(high) > 0) {
			return high;
		}
		return amount;
	}
	
	/**
	 * Override to change exception handling.
	 * @param e
	 */
	protected void onException(Throwable e) {
		e.printStackTrace();
	}
	/**
	 * Debugging method.
	 * @param debugPrint What to print when a discrepancy is detected.
	 * @throws IOException
	 */
	protected void checkInequality(String debugPrint) throws IOException {
		DDState real = api.getFreshState(cheatingVision);
		DDState simulation = state;
		
		boolean discrepancyDetected = false;
		for (int y = 0; y < 20; ++y) {
			for (int x = 0; x < 20; ++x) {
				DDObject realObject = real.getObjectAt(x, y);
				DDObject simObject = simulation.getObjectAt(x, y);
				if (!realObject.equals(simObject)) {
					if (!discrepancyDetected) {
						discrepancyDetected = true;
						System.out.println("DISCREPANCY DETECTED!");
						System.out.println("After action: " + debugPrint);
					}
					System.out.println("At location x=" + x + " y=" + y);
					System.out.println("Real:       " + realObject);
					System.out.println("Simulation: " + simObject);
					System.out.println();
				}
			}
		}
		
		if (discrepancyDetected) {
			System.out.println("BREAKPOINT!");
		}
	}
	
	/**
	 * Sets beast types encounterable in the dungeons.
	 * @param beastTypes
	 */
	protected void setGeneratedBeasts(Beast.Type... beastTypes) {
		customBeasts = Arrays.asList(beastTypes);
	}
	/**
	 * Restarts the game.
	 * @throws IOException
	 */
	protected void restartGame() throws IOException {
		api.startGame(heroClass, heroRace, gameType, false, cheatingVision);
	}
	
	/**
	 * Called after the first run is started.
	 */
	protected void firstRunStart() {
	}
	
	/**
	 * Called at the start of every run.
	 */
	protected void runStart() {
	}
	/**
	 * Called at the end of every run.
	 */
	protected void runEnd() {};
	
	
	/**
	 * Main logic method, the bot should perform one step
	 * and return. Speed of these steps can be modified by setting
	 * the millisBetween attribute.
	 * 
	 * @throws IOException
	 */
	public abstract void step() throws IOException;
	
	/**
	 * Returns whether the bot has no more actions in this dungeon.
	 * Feel free to override this method, but it may be easier
	 * just to set the "finished" variable to false.
	 * 
	 * @return
	 */
	public boolean isFinished() {
		return finished;
	}
	
	/**
	 * Runs the bot once.
	 */
	public final void run() {
		innerRun(false);
	}
	/**
	 * Runs the bot in a loop. Also creates a GUI to be able to end the
	 * bot when not run from command line.
	 */
	public final void loop() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShowGUI();
			}
		});
		innerRun(true);
	}
	
	/**
	 * Miniature GUI to run the bot from outside command line.
	 */
	private void createAndShowGUI() {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		
		Container pane = frame.getContentPane();
		
		JLabel label = new JLabel(name);
		pane.add(label);
		
		frame.pack();
		frame.setVisible(true);
	}
	
	private void innerRun(boolean looped) {
		try {
			api.connect();
			if (customBeasts != null)
				api.setGeneratedBeasts(customBeasts);
			try {
				do {
					api.startGame(heroClass, heroRace, gameType, false, cheatingVision);
					state = api.getState();
					finished = false;
					int steps = 0;
					long runStart = System.currentTimeMillis();
					if (runs == 0)
						firstRunStart();
					runStart();
					++runs;
					while (!isFinished()) {
						long beforeStep = System.currentTimeMillis();
						step();
						++steps;
						long stepTime = System.currentTimeMillis() - beforeStep;
						long wait = clamp(millisBetween - stepTime, 0L, Long.MAX_VALUE);
						if (wait > 0)
							Thread.sleep(wait);
					}
					runEnd();
					System.out.println(api.getState().getHero().toString());
					long totalMillis = System.currentTimeMillis() - runStart;
					System.out.println("Steps: " + steps + " Time: " + totalMillis / 1000.0 + "s");
					if (millisBetweenRuns > 0)
						Thread.sleep(millisBetweenRuns);
				} while (looped);
				
			} finally {
				api.close();
			}
		} catch (Exception e) {
			onException(e);
		}
	}

	
}
