package cz.woitee.ddframework;

import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;


/**
 * An altar object in the game.
 * @author woitee
 *
 */
public class Altar extends DDObject implements Serializable {
	private static final long serialVersionUID = 1L;
	private God god;
	public Altar(God god, Point location) {
		super(location);
		objectType = DDObjectType.ALTAR;
		this.god = god;
	}
	
	/**
	 * A god name.
	 * @author woitee
	 */
	public enum God {
		BinlorIronshield("BINLOR"),
		Dracul("DRACUL"),
		Earthmother("EARTHMOTHER"),
		GlowingGuardian("GLOWING"),
		JehoraJeheyu("JEHORA"),
		MysteraAnnur("MYSTERA"),
		Pactmaker("PACTMAKER"),
		Taurog("TAUROG"),
		TikkiTooki("TIKKI"),
		None("NONE");
		
		private String name;
		private God(String name) {
			this.name = name;
		}
		
		/**
		 * Custom overriden, returns value ready to send over connection
		 * to Desktop Dungeons with API.
		 */
		@Override
		public String toString() {
			return name;
		}
		
		/**
		 * Returns the first three letters of god name.
		 * @return
		 */
		public String toLayoutString() {
			return name.substring(0, 3);
		}
		
		/** Parses a god value from string.
		 * Return null if not possible to parse.
		 * @param s The string to parse.
		 * @return
		 */
		public static God parse(String s) {
			for (God god : God.values()) {
				if (god.toString().equals(s)) {
					return god;
				}
			}
			return null;
		}
	}
	/**
	 * Returns the name of God this Altar belongs to.
	 * @return
	 */
	public God getGodType() {
		return god;
	}
	/**
	 * Returns the list of boons you can get from this god.
	 * @return
	 */
	public Collection<Boon> getBoons() {
		return new ArrayList<Boon>();
	}
	
	/**
	 * Returns whether the hero can convert to this god.
	 * @return
	 */
	public boolean canConvert() {
		return false;
	}
	
	@Override
	public Object clone() {
		return new Altar(this.god, (Point) this.location.clone());
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Altar)) {
			return false;
		}
		
		Altar other = (Altar)o;
		return this.location.equals(other.location) &&
				this.god == other.god;
	}
	
	@Override
	public String toString() {
		return "Altar of " + god + " at " + location;
	}
	
	@Override
	public String toLayoutString() {
		return "A_" + god.toLayoutString();
	}
	
	public static Altar parse(String s, Point location) {
		String[] split = s.split(" ");
		if (split.length != 2 || !split[0].equals("ALTAR")) {
			return null;
		}
		God god = God.parse(split[1]);
		if (god == null) {
			return null;
		}
		return new Altar(god, location);
	}
}
